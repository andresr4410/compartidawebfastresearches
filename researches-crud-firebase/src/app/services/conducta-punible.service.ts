import { Injectable } from '@angular/core';
import { ConductaPunible } from '../models/conductaPunible';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ConductaPunibleService {
  private dbPath = '/conductaPunible';

  conductaPunibleRef:AngularFirestoreCollection<ConductaPunible> = null;

  constructor(private db:AngularFirestore) {
    this.conductaPunibleRef = db.collection(this.dbPath);
   }

   createConductaPunible(conductaPunible:ConductaPunible):void{
     this.conductaPunibleRef.add({...conductaPunible});
   }

   updateConductaPunible(key:string,value:any):Promise<void>{
     return this.conductaPunibleRef.doc(key).update(value);
   }

   deleteConductaPunible(key:string):Promise<void>{
     return this.conductaPunibleRef.doc(key).delete();
   }

   getConductaPunibleList():AngularFirestoreCollection<ConductaPunible>{
     return this.conductaPunibleRef;
   }
}
