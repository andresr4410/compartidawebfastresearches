import { Injectable } from '@angular/core';
import { PagMisionTrabajo } from '../models/pagMisionTrabajo';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class PagMisionTrabajoService {
  private dbPath = '/pagMisionTrabajo';
  
  pagMisionTrabajorRef: AngularFirestoreCollection<PagMisionTrabajo> = null;

  constructor(private db:AngularFirestore) {
    this.pagMisionTrabajorRef = db.collection(this.dbPath);
   }

   createpagMisionTrabajo(pagMisionTrabajo:PagMisionTrabajo):void{
      this.pagMisionTrabajorRef.add({...pagMisionTrabajo});
   }

   updatepagMisionTrabajo(key:string, value:any):Promise<void>{
    return this.pagMisionTrabajorRef.doc(key).update(value);
   }

   deletepagMisionTrabajo(key:string):Promise<void>{
    return this.pagMisionTrabajorRef.doc(key).delete();
   }

   getpagMisionTrabajo():AngularFirestoreCollection<PagMisionTrabajo>{
    return this.pagMisionTrabajorRef;
   }
}
