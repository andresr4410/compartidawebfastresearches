import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { DefensorPublico } from '../models/DefensorPublico';


@Injectable({
  providedIn: 'root'
})
export class DefensorPublicoService {

  private dbPath = '/defensorPublico';
  
  defensorPublicoRef: AngularFirestoreCollection<DefensorPublico> = null;

  constructor(private db:AngularFirestore) {
    this.defensorPublicoRef = db.collection(this.dbPath);
   }

   createDefensorPublico(defensorPublico:DefensorPublico):void{
      this.defensorPublicoRef.add({...defensorPublico});
   }

   updateDefensorPublico(key:string, value:any):Promise<void>{
    return this.defensorPublicoRef.doc(key).update(value);
   }

   deleteDefensorPublico(key:string):Promise<void>{
    return this.defensorPublicoRef.doc(key).delete();
   }

   getDefensorPublicoList():AngularFirestoreCollection<DefensorPublico>{
    return this.defensorPublicoRef;
   }

}
