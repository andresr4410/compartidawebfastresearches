import { Injectable } from '@angular/core';
import { ItemInforme } from '../models/item-informe';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})

export class ItemInformeService {
  private dbPath = '/itemInformeWeb';
  
  itemInformeRef: AngularFirestoreCollection<ItemInforme> = null;

  constructor(db:AngularFirestore) {
    this.itemInformeRef = db.collection(this.dbPath);
   }

   createItemInforme(itemInforme:ItemInforme):void{
      this.itemInformeRef.add({...itemInforme});
      
   }

   updateItemInforme(key:string, value:any):Promise<void>{
    return this.itemInformeRef.doc(key).update(value);
   }

   deleteItemInforme(key:string):Promise<void>{
    return this.itemInformeRef.doc(key).delete();
   }

   getItemInformeList():AngularFirestoreCollection<ItemInforme>{
    return this.itemInformeRef;
   }
}
