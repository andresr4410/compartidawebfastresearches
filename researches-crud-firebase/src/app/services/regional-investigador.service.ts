import { Injectable } from '@angular/core';
import { RegionalInvestigador } from '../models/regionalInvestigador';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class RegionalInvestigadorService {
  private dbPath = '/regionalInvestigador';
  
  regionalInvestigadorRef: AngularFirestoreCollection<RegionalInvestigador> = null;

  constructor(private db:AngularFirestore) {
    this.regionalInvestigadorRef = db.collection(this.dbPath);
   }

   createRegionalInvestigador(regionalInvestigador:RegionalInvestigador):void{
      this.regionalInvestigadorRef.add({...regionalInvestigador});
   }

   updateRegionalInvestigador(key:string, value:any):Promise<void>{
    return this.regionalInvestigadorRef.doc(key).update(value);
   }

   deleteRegionalInvestigador(key:string):Promise<void>{
    return this.regionalInvestigadorRef.doc(key).delete();
   }

   getRegionalInvestigador():AngularFirestoreCollection<RegionalInvestigador>{
    return this.regionalInvestigadorRef;
   }
}
