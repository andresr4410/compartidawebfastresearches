import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Programa } from '../models/programa';

@Injectable({
  providedIn: 'root'
})
export class ProgramaService {

  private dbPath = '/programa';

  programaRef:AngularFirestoreCollection<Programa> = null;

  constructor(private db:AngularFirestore) {
    this.programaRef = db.collection(this.dbPath);
   }

   createPrograma(programa:Programa):void{
     this.programaRef.add({...programa});
   }

   updatePrograma(key:string,value:any):Promise<void>{
     return this.programaRef.doc(key).update(value);
   }

   deletePrograma(key:string):Promise<void>{
     return this.programaRef.doc(key).delete();
   }

   getProgramaList():AngularFirestoreCollection<Programa>{
     return this.programaRef;
   }

}
