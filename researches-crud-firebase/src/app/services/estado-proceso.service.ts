import { Injectable } from '@angular/core';
import { EstadoProceso } from '../models/estadoProceso';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class EstadoProcesoService {

  private dbPath = '/estadoProceso';
  
  estadoProcesoRef: AngularFirestoreCollection<EstadoProceso> = null;

  constructor(private db:AngularFirestore) {
    this.estadoProcesoRef = db.collection(this.dbPath);
   }

   createEstadoProceso(estadoProceso:EstadoProceso):void{
      this.estadoProcesoRef.add({...estadoProceso});
   }

   updateEstadoProceso(key:string, value:any):Promise<void>{
    return this.estadoProcesoRef.doc(key).update(value);
   }

   deleteEstadoProceso(key:string):Promise<void>{
    return this.estadoProcesoRef.doc(key).delete();
   }

   getEstadoProcesoList():AngularFirestoreCollection<EstadoProceso>{
    return this.estadoProcesoRef;
   }

}
