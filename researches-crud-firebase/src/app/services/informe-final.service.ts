import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { DefensorPublico } from '../models/DefensorPublico';
import { InformeFinal } from '../models/informe-final';

@Injectable({
  providedIn: 'root'
})
export class InformeFinalService {
  private dbPath = '/informeFinal';
  
  informeFinalRef: AngularFirestoreCollection<InformeFinal> = null;

  constructor(db:AngularFirestore) {
    this.informeFinalRef = db.collection(this.dbPath);
   }

   createInformeFinal(informeFinal:InformeFinal):void{
      this.informeFinalRef.add({...informeFinal});
      
   }

   updateInformeFinal(key:string, value:any):Promise<void>{
    return this.informeFinalRef.doc(key).update(value);
   }

   deleteInformeFinal(key:string):Promise<void>{
    return this.informeFinalRef.doc(key).delete();
   }

   getInformeFinalList():AngularFirestoreCollection<InformeFinal>{
    return this.informeFinalRef;
   }

}
