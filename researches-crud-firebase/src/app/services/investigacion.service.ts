import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Investigacion } from '../models/investigacion';

@Injectable({
  providedIn: 'root'
})
export class InvestigacionService {

  private dbPath = '/investigacion';
  
  investigacionRef: AngularFirestoreCollection<Investigacion> = null;

  constructor(db:AngularFirestore) {
    this.investigacionRef = db.collection(this.dbPath);
   }

   createInvestigacion(investigacion:Investigacion):void{
      this.investigacionRef.add({...investigacion});
   }

   updateInvestigacion(key:string, value:any):Promise<void>{
    return this.investigacionRef.doc(key).update(value);
   }

   deleteInvestigacion(key:string):Promise<void>{
    return this.investigacionRef.doc(key).delete();
   }

   getInvestigacionList():AngularFirestoreCollection<Investigacion>{
    return this.investigacionRef;
   }
}
