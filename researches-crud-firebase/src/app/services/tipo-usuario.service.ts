import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { TipoUsuario } from '../models/tipoUsuario';

@Injectable({
  providedIn: 'root'
})
export class TipoUsuarioService {
  private dbPath = '/tipoUsuario';

  tipoUsuarioRef:AngularFirestoreCollection<TipoUsuario> = null;

  constructor(private db:AngularFirestore) {
    this.tipoUsuarioRef = db.collection(this.dbPath);
   }

   createTipoUsuario(tipoUsuario:TipoUsuario):void{
     this.tipoUsuarioRef.add({...tipoUsuario});
   }

   updateTipoUsuario(key:string,value:any):Promise<void>{
     return this.tipoUsuarioRef.doc(key).update(value);
   }

   deleteTipoUsuario(key:string):Promise<void>{
     return this.tipoUsuarioRef.doc(key).delete();
   }

   getTipoUsuarioList():AngularFirestoreCollection<TipoUsuario>{
     return this.tipoUsuarioRef;
   }
}