import { Injectable } from '@angular/core';
 
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
 
// For try-catch mechanism.
import { Observable } from 'rxjs/internal/Observable';
import { Router } from '@angular/router';
import { UsuarioService } from './usuario.service';
import { Usuario } from '../models/usuario';
import Swal from 'sweetalert2';
 
@Injectable({
  providedIn: 'root'
})
 
export class AuthService {
 
  user: Observable<firebase.User>;
  err: String;
  bandera:Boolean;
  tipoError:string;
   
  constructor(private firebaseAuth: AngularFireAuth,
    public router:Router,
    public usuarioService:UsuarioService) {
    this.user = firebaseAuth.authState;
  }
 
  
  ngOnInit() {
    
  }
  // To sign up a new user.
  signup(usuario:Usuario) {
    if(usuario.idTipoUsuario==-1 && usuario.email==undefined && usuario.clave==undefined ){
      this.tipoError = "Asegurese de digitar todos los campos ";
      Swal.fire('No se pudo guardar el usuario', this.tipoError, 'error');
    }else if(usuario.idTipoUsuario==-1){
      this.tipoError = "No selecciono un tipo de usuario del sistema !! ";
      Swal.fire('No se pudo guardar el usuario', this.tipoError, 'error');
    }else if(usuario.email==undefined){
      this.tipoError = "No digito un correo, esta vacio ! ";
      Swal.fire('No se pudo guardar el usuario', this.tipoError, 'error');
    }else if(usuario.clave==undefined){
      this.tipoError = "No digito una clave, esta vacio ! ";
      Swal.fire('No se pudo guardar el usuario', this.tipoError, 'error');
    }else{
      this.firebaseAuth
      .auth
      .createUserWithEmailAndPassword(usuario.email, usuario.clave)
      .then(value => {
        this.tipoError ="Todo correcto";
        Swal.fire('Se guardo el usuario '+'con email: '+ value.user.email+' ', this.tipoError, 'success');
        this.usuarioService.usuarioRef.add({...usuario});
      })
      .catch(error => {
        Swal.fire('No se pudo guardar el usuario: ','Correo y/o contraseña no validos o  ya registrados ', 'error');
      });
    } 
  }
 
  // To login a valid user.
  login(emailAddress: string, password: string) {
      this.firebaseAuth
      .auth
      .signInWithEmailAndPassword(emailAddress, password)
      .then(value => {
        this.usuarioService.getUsuarioList().get().subscribe(response=>{
          var login = {
            email:'' ,
            idTipoUsuario:'',
            };
          response.forEach(data=>{
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 1){
              this.router.navigate(['moduloAuxiliarAdministrativo']);
              login.email = data.data().email;
              login.idTipoUsuario = "1";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 2){
              this.router.navigate(['moduloIngeniero']);
              login.email = data.data().email;
              login.idTipoUsuario = "2";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 3){
              this.router.navigate(['moduloCoordinadorPag']);
              login.email = data.data().email;
              login.idTipoUsuario = "3";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 7){
              this.router.navigate(['moduloDefensorPublico']);
              login.email = data.data().email;
              login.idTipoUsuario = "7";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 4){
              this.router.navigate(['moduloTecnicoCriminalistica']);
              login.email = data.data().email;
              login.idTipoUsuario = "4";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 6){
              this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
              login.email = data.data().email;
              login.idTipoUsuario = "6";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
            if(emailAddress.localeCompare(data.data().email) == 0 && password.localeCompare(data.data().clave) == 0 
            && data.data().idTipoUsuario == 5){
              this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
              login.email = data.data().email;
              login.idTipoUsuario = "5";
              localStorage.setItem('usuario',JSON.stringify(login));
            }
          });
        });
      })
      .catch(error => {
        this.err = error;
        console.log('Something went wrong:', error);
      });
    }


  borrarUsuario(correo:string,clave:string){
    firebase.auth().signInWithEmailAndPassword(correo, clave)
    .then(function (info) {
       var user = firebase.auth().currentUser;
       user.delete();
    });
  }
 
  // To logout an authenticated user.
  logout() {
    this.firebaseAuth
      .auth
      .signOut();
  }

}