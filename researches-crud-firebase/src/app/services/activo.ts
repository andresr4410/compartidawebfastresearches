import { Injectable } from '@angular/core';
import { Estado } from '../models/estado';

@Injectable({
    providedIn: 'root'
  })
  export class ActivoService {
    public listaActivo:Estado[];
    constructor() { 
      this.listaActivo=[
        {activo:true,nombre:'Activo'},
        {activo:false,nombre:'Inactivo'}
      ]
    }
}