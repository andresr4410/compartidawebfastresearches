import { Injectable } from '@angular/core';
import { FileUpload } from '../models/FileUpload';
import {AngularFirestoreCollection} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { AngularFireStorage } from 'angularfire2/storage';
import { finalize } from 'rxjs/operators';
import {AngularFirestore} from '@angular/fire/firestore';
import { FileUpload2 } from '../models/fileUpload2';

@Injectable({
  providedIn: 'root'
})
export class FileServicesService {
  private basePath = '/uploads';
  
  fileUpload2 = new FileUpload2();

  //Url que corresponde a las evidencias del investigador criminal y documentos
  urlRetrieve:string="";
 
  uploadRef: AngularFirestoreCollection<FileUpload2> = null;
 

  constructor(private storage: AngularFireStorage,
    private db2:AngularFirestore) {
      this.uploadRef = db2.collection(this.basePath);
  }
   
  pushFileToStorage(fileUpload: FileUpload): Observable<number> {
    const filePath = `${this.basePath}/${fileUpload.file.name}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath, fileUpload.file);
 
    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageRef.getDownloadURL().subscribe(downloadURL => {
          fileUpload.url = downloadURL;
          fileUpload.name = fileUpload.file.name;
          this.fileUpload2.name = fileUpload.file.name;
          this.fileUpload2.url = downloadURL;
          this.saveFileData2(this.fileUpload2);
        });
      })
    ).subscribe();
    return uploadTask.percentageChanges();
  }

  private saveFileData2(fileUpload2:FileUpload2){
    this.uploadRef.add({...fileUpload2});
  }


  deleteFileUpload(fileUpload2: FileUpload2) {
    this.deleteFileDatabase2(fileUpload2.key)
    .then(() => {
      this.deleteFileStorage(fileUpload2.name);
    })
    .catch(error => console.log(error));
  }

  deleteFileDatabase2(key:string):Promise<void>{
    return this.uploadRef.doc(key).delete();
  }

 
  private deleteFileStorage(name: string) {
    const storageRef = this.storage.ref(this.basePath);
    storageRef.child(name).delete();
  }

  getUploadList():AngularFirestoreCollection<FileUpload2>{
    return this.uploadRef;
  }

  public async getUrlRetrieve(snap: firebase.storage.UploadTaskSnapshot) {
    const url = await snap.ref.getDownloadURL();
    this.urlRetrieve = url;  //store the URL
    return this.urlRetrieve;
  }
}


