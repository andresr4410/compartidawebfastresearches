import { Injectable } from '@angular/core';
import { Ciudadano } from '../models/ciudadano';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CiudadanoService {

  private dbPath = '/ciudadano';

  ciudadanoRef:AngularFirestoreCollection<Ciudadano> = null;

  constructor(private db:AngularFirestore) {
    this.ciudadanoRef = db.collection(this.dbPath);
   }

   createCiudadano(ciudadano:Ciudadano):void{
     this.ciudadanoRef.add({...ciudadano});
   }

   updateCiudadano(key:string,value:any):Promise<void>{
     return this.ciudadanoRef.doc(key).update(value);
   }

   deleteCiudadano(key:string):Promise<void>{
     return this.ciudadanoRef.doc(key).delete();
   }

   getCiudadanoList():AngularFirestoreCollection<Ciudadano>{
     return this.ciudadanoRef;
   }
}
