import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { PAG } from '../models/pag';

@Injectable({
  providedIn: 'root'
})
export class PagService {
  private dbPath = '/pag';
  
  pagRef: AngularFirestoreCollection<PAG> = null;

  constructor(private db:AngularFirestore) {
    this.pagRef = db.collection(this.dbPath);
   }

   createPag(pag:PAG):void{
      this.pagRef.add({...pag});
   }

   updatePag(key:string, value:any):Promise<void>{
    return this.pagRef.doc(key).update(value);
   }

   deletePag(key:string):Promise<void>{
    return this.pagRef.doc(key).delete();
   }

   getPagList():AngularFirestoreCollection<PAG>{
    return this.pagRef;
   }
}
