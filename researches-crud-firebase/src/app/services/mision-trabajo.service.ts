import { Injectable } from '@angular/core';
import { MisionTrabajo } from '../models/misionTrabajo';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class MisionTrabajoService {
  private dbPath = '/misionTrabajo';
  
  misionTrabajoRef: AngularFirestoreCollection<MisionTrabajo> = null;

  constructor(private db:AngularFirestore) {
    this.misionTrabajoRef = db.collection(this.dbPath);
   }

   createMisionTrabajo(misionTrabajo:MisionTrabajo):void{
      this.misionTrabajoRef.add({...misionTrabajo});
   }

   updateMisionTrabajo(key:string, value:any):Promise<void>{
    return this.misionTrabajoRef.doc(key).update(value);
   }

   deleteMisionTrabajo(key:string):Promise<void>{
    return this.misionTrabajoRef.doc(key).delete();
   }

   getMisionTrabajoList():AngularFirestoreCollection<MisionTrabajo>{
    return this.misionTrabajoRef;
   }
}
