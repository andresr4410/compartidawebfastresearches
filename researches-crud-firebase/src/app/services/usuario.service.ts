import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private dbPath = '/usuario';
  usuarioRef:AngularFirestoreCollection<Usuario> = null;

  constructor(public db:AngularFirestore) {
    this.usuarioRef = db.collection(this.dbPath);
   }

   createUsuario(usuario:Usuario){
      this.usuarioRef.add({...usuario});
   }

   updateUsuario(key:string,value:any):Promise<void>{
     return this.usuarioRef.doc(key).update(value);
   }

   deleteUsuario(key:string):Promise<void>{
     return this.usuarioRef.doc(key).delete();
   }

   getUsuarioList():AngularFirestoreCollection<Usuario>{
     return this.usuarioRef;
   }


}
