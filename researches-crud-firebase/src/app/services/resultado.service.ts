import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Resultado } from '../models/resultado';

@Injectable({
  providedIn: 'root'
})
export class ResultadoService {
  private dbPath = '/resultado';
  
  resultadoRef: AngularFirestoreCollection<Resultado> = null;

  constructor(private db:AngularFirestore) {
    this.resultadoRef = db.collection(this.dbPath);
   }

   createResultado(resultado:Resultado):void{
      this.resultadoRef.add({...resultado});
   }

   updateResultado(key:string, value:any):Promise<void>{
    return this.resultadoRef.doc(key).update(value);
   }

   deleteResultado(key:string):Promise<void>{
    return this.resultadoRef.doc(key).delete();
   }

   getResultadoList():AngularFirestoreCollection<Resultado>{
    return this.resultadoRef;
   }
}
