import { Injectable } from '@angular/core';
import { FileUpload2 } from '../models/fileUpload2';
import {AngularFirestoreCollection} from '@angular/fire/firestore';
import {AngularFirestore} from '@angular/fire/firestore';
import { FileUpload } from '../models/FileUpload';
import { Observable } from 'rxjs';
import { AngularFireStorage } from 'angularfire2/storage';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MisionFileServicesService {
  private basePathMision = '/misionTrabajoFiles';

  fileMisionTrabajo = new FileUpload2();

  misionTrabajoFilesRef:AngularFirestoreCollection<FileUpload2> = null;
  constructor(public db3:AngularFirestore,
    private storagee: AngularFireStorage) {
    this.misionTrabajoFilesRef = db3.collection(this.basePathMision);
   }
   
   pushFileToStorageMision(fileUploadMision: FileUpload): Observable<number> {
    const filePath = `${this.basePathMision}/${fileUploadMision.file.name}`;
    const storageReferencia = this.storagee.ref(filePath);
    const uploadTask = this.storagee.upload(filePath, fileUploadMision.file);
 
    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageReferencia.getDownloadURL().subscribe(downloadURL => {
          fileUploadMision.url = downloadURL;
          fileUploadMision.name = fileUploadMision.file.name;
          this.fileMisionTrabajo.name = fileUploadMision.file.name;
          this.fileMisionTrabajo.url = downloadURL;
          this.saveFileMisionTrabajo(this.fileMisionTrabajo);
        });
      })
    ).subscribe();
    return uploadTask.percentageChanges();
  }

  private saveFileMisionTrabajo(fileMisionTrabajo:FileUpload2){
    this.misionTrabajoFilesRef.add({...fileMisionTrabajo});
  }

  deleteFileMisionTrabajo(fileMisionTrabajo: FileUpload2) {
    this.deleteFileDatabaseMisionTrabajo(fileMisionTrabajo.key)
    .then(() => {
      this.deleteFileStorage(fileMisionTrabajo.name);
    })
    .catch(error => console.log(error));
  }

  private deleteFileStorage(name: string) {
    const storageReferencia = this.storagee.ref(this.basePathMision);
    storageReferencia.child(name).delete();
  }

  deleteFileDatabaseMisionTrabajo(key:string):Promise<void>{
    return this.misionTrabajoFilesRef.doc(key).delete();
  }

  getUploadList():AngularFirestoreCollection<FileUpload2>{
    return this.misionTrabajoFilesRef;
  }

}
