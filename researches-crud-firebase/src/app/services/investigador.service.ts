import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Investigador } from '../models/investigador';

@Injectable({
  providedIn: 'root'
})
export class InvestigadorService {

  private dbPath = '/investigador';
  
  investigadorRef: AngularFirestoreCollection<Investigador> = null;

  constructor(private db:AngularFirestore) {
    this.investigadorRef = db.collection(this.dbPath);
   }

   createInvestigador(investigador:Investigador):void{
      this.investigadorRef.add({...investigador});
   }

   updateInvestigador(key:string, value:any):Promise<void>{
    return this.investigadorRef.doc(key).update(value);
   }

   deleteInvestigador(key:string):Promise<void>{
    return this.investigadorRef.doc(key).delete();
   }

   getInvestigadorList():AngularFirestoreCollection<Investigador>{
    return this.investigadorRef;
   }
}
