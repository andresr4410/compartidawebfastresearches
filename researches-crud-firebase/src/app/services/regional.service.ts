import { Injectable } from '@angular/core';
import {AngularFirestore,AngularFirestoreCollection} from '@angular/fire/firestore';
import { Regional } from '../models/regional';

@Injectable({
  providedIn: 'root'
})
export class RegionalService {
  private dbPath = '/regional';
  
  regionalRef: AngularFirestoreCollection<Regional> = null;

  constructor(private db:AngularFirestore) {
    this.regionalRef = db.collection(this.dbPath);
   }

   createregional(regional:Regional):void{
      this.regionalRef.add({...regional});
   }

   updateregional(key:string, value:any):Promise<void>{
    return this.regionalRef.doc(key).update(value);
   }

   deleteregional(key:string):Promise<void>{
    return this.regionalRef.doc(key).delete();
   }

   getregionalList():AngularFirestoreCollection<Regional>{
    return this.regionalRef;
   }
}
