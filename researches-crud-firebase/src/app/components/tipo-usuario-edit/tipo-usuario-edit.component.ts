import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoUsuarioService } from 'src/app/services/tipo-usuario.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-tipo-usuario-edit',
  templateUrl: './tipo-usuario-edit.component.html',
  styleUrls: ['./tipo-usuario-edit.component.css']
})
export class TipoUsuarioEditComponent implements OnInit {

  public tipoUsuario:any;
  public idTipoUsuario:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public tipoUsuarioService:TipoUsuarioService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadUsuario();
  }

  loadUsuario(){
    let params=this.activatedRoute.params['_value'];
    this.idTipoUsuario = params.tipoUsuarioId;
    var tipoUsuario = {
      rol:'',
      idTipoUsuario:0,
      activo:true
    }
    tipoUsuario.idTipoUsuario = params.tipoUsuarioId;
    this.tipoUsuarioService.getTipoUsuarioList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.idTipoUsuario == value.idTipoUsuario){
          tipoUsuario.rol = value.rol;
          tipoUsuario.activo = value.activo;
          this.tipoUsuario = tipoUsuario;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.tipoUsuarioService.getTipoUsuarioList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.idTipoUsuario == value.idTipoUsuario){
          var key = document.id;
          this.tipoUsuarioService.updateTipoUsuario(key,this.tipoUsuario);
          Swal.fire('Se actualizo tipo usuario con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
