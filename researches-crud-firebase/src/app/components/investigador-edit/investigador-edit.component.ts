import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-investigador-edit',
  templateUrl: './investigador-edit.component.html',
  styleUrls: ['./investigador-edit.component.css']
})
export class InvestigadorEditComponent implements OnInit {
  public investigador:any;
  public cedula:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public investigadorService:InvestigadorService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadInvestigador();
  }

  loadInvestigador(){
    let params=this.activatedRoute.params['_value'];
    this.cedula = params.cedulaInvestigador;
    var investigador = {
      nombre:'',
      apellido:'',
      profesion:'',
      cargo:'',
      telefono:0,
      direccion:'',
      email:'',
      cedula:'',
      observacion:''
    }
    investigador.cedula = this.cedula;
    this.investigadorService.getInvestigadorList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          investigador.nombre = value.nombre;
          investigador.apellido = value.apellido;
          investigador.cargo = value.cargo;
          investigador.telefono = value.telefono;
          investigador.direccion = value.direccion;
          investigador.email = value.email;
          this.investigador = investigador;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.investigadorService.getInvestigadorList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          var key = document.id;
          this.investigadorService.updateInvestigador(key,this.investigador);
          Swal.fire('Se actualizo investigador con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
