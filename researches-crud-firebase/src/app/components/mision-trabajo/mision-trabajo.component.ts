import { Component, OnInit, SimpleChanges,OnChanges, Input } from '@angular/core';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { RegionalService } from 'src/app/services/regional.service';
import { ResultadoService } from 'src/app/services/resultado.service';
import { ConductaPunibleService } from 'src/app/services/conducta-punible.service';
import { EstadoProcesoService } from 'src/app/services/estado-proceso.service';
import { PagService } from 'src/app/services/pag.service';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { InvestigacionService } from 'src/app/services/investigacion.service';
import { CiudadanoService } from 'src/app/services/ciudadano.service';
import { ProgramaService } from 'src/app/services/programa.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { FileUpload } from 'src/app/models/FileUpload';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import Swal from 'sweetalert2';
import { Ciudadano } from 'src/app/models/ciudadano';
@Component({
  selector: 'app-mision-trabajo',
  templateUrl: './mision-trabajo.component.html',
  styleUrls: ['./mision-trabajo.component.css']
})
export class MisionTrabajoComponent implements OnInit {
  selectedFilesMision: FileList;
  currentFileUploadMision: FileUpload;
  percentageMision: number;
  diasParaElVencimiento:number=0;
 
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public mision:MisionTrabajo = new MisionTrabajo();
  public tipoError:string = "";
  public programas:string[];
  public ciudadanos:string[];
  public investigaciones:string[];
  public investigadores:string[];
  public defensoresPublicos:string[];
  public pags:string[];
  public regionales:string[];
  public estadoProcesos:string[];
  public conductasPunibles:string[];
  public resultados:string[];
  public encontroCiudadano:boolean = false;

  filteredRegional: Observable<string[]>;
  filteredPrograma: Observable<string[]>;
  filteredCiudadano: Observable<string[]>;
  filteredInvestigador: Observable<string[]>;
  filteredInvestigacion: Observable<string[]>;
  filteredDefensorPublico:Observable<string[]>;
  filteredResultado:Observable<string[]>;
  filteredPAG:Observable<string[]>;
  filteredEstadoProceso:Observable<string[]>;
  filteredConductaPunible:Observable<string[]>;
  myControlRegional: FormControl;
  myControlPrograma:FormControl;
  myControlCiudadano:FormControl;
  myControlInvestigador:FormControl;
  myControlInvestigacion:FormControl;
  myControlDefensorPublico:FormControl;
  myControlResultado:FormControl;
  myControlPAG:FormControl;
  myControlEstadoProceso:FormControl;
  myControlConductaPunible:FormControl;
  variable:string="";
  validarMision:boolean =true;
  nuevoCiudadano:Ciudadano = new Ciudadano();
  

  constructor(
    public misionTrabajoService:MisionTrabajoService,
    public router:Router,
    public authService:AuthService,
    public regionalService:RegionalService,
    public resultadoService:ResultadoService,
    public conductaPunibleService:ConductaPunibleService,
    public estadoProcesoService:EstadoProcesoService,
    public pagService:PagService,
    public defensorPublicoService:DefensorPublicoService,
    public investigacionService:InvestigacionService,
    public ciudadanoService:CiudadanoService,
    public programaService:ProgramaService,
    public investigadorService:InvestigadorService,
    public misionFilesService:MisionFileServicesService) { 
    }
  
    
  ngOnInit(){
    this.myControlPrograma = new FormControl('');
    this.myControlRegional = new FormControl('');
    this.myControlCiudadano = new FormControl('');
    this.myControlInvestigador = new FormControl('');
    this.myControlInvestigacion = new FormControl('');
    this.myControlDefensorPublico = new FormControl('');
    this.myControlConductaPunible = new FormControl('');
    this.myControlResultado = new FormControl('');
    this.myControlPAG = new FormControl('');
    this.myControlEstadoProceso = new FormControl('');
    this.programas = ['',''];
    this.sacarProgramas();
    this.ciudadanos = ['',''];
    this.sacarCiudadanos();
    this.regionales = ['',''];
    this.sacarRegionales();
    this.defensoresPublicos = ['',''];
    this.sacarDefensoresPublicos();
    this.pags = ['',''];
    this.sacarPags();
    this.investigaciones= ['',''];
    this.sacarInvestigaciones();
    this.regionales = ['',''];
    this.sacarRegionales();
    this.estadoProcesos = ['',''];
    this.sacarEstadoProcesos ();
    this.conductasPunibles = ['',''];
    this.sacarConductasPunibles();
    this.resultados = ['',''];
    this.sacarResultados();   
    this.investigadores = ['',''];
    this.sacarInvestigadores(); 
    
  }

  selectFileMision(eventMision) {
    this.selectedFilesMision = eventMision.target.files;
  }

  uploadMision() {
    //const fileMision = this.selectedFilesMision.item(1);
    this.selectedFilesMision = undefined;
 
    //this.currentFileUploadMision = new FileUpload(fileMision);
    this.misionFilesService.pushFileToStorageMision(this.currentFileUploadMision).subscribe(
      percentageMision => {
        this.percentageMision = Math.round(percentageMision);
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.mision.misionTrabajo = this.mision.radicadoInterno+"-";
  }

  private _filterRegional(valueRegional: string): string[] {
    const filterValueRegional = valueRegional.toLowerCase();
    return this.regionales.filter(optionRegional => optionRegional.toLowerCase().indexOf(filterValueRegional)===0);
  }

  private _filterPrograma(valuePrograma: string): string[] {
    const filterValuePrograma = valuePrograma.toLowerCase();
    return this.programas.filter(optionPrograma => optionPrograma.toLowerCase().indexOf(filterValuePrograma)===0);
  }

  private _filterCiudadano(valueCiudadano: string): string[] {
    const filterValueCiudadano = valueCiudadano.toLowerCase();
    return this.ciudadanos.filter(optionCiudadano => optionCiudadano.toLowerCase().indexOf(filterValueCiudadano)===0);
  }

  private _filterInvestigadores(valueInvestigador: string): string[] {
    const filterValueInvestigador = valueInvestigador.toLowerCase();
    return this.investigadores.filter(optionInvestigador => optionInvestigador.toLowerCase().indexOf(filterValueInvestigador)===0);
  }

  private _filterInvestigacion(valueInvestigacion: string): string[] {
    const filterValueInvestigacion = valueInvestigacion.toLowerCase();
    return this.investigaciones.filter(optionInvestigacion => optionInvestigacion.toLowerCase().indexOf(filterValueInvestigacion)===0);
  }

  private _filterDefensorPublico(valueDefensorPublico: string): string[] {
    const filterValueDefensorPublico = valueDefensorPublico.toLowerCase();
    return this.defensoresPublicos.filter(OptionDefensorPublico => OptionDefensorPublico.toLowerCase().indexOf(filterValueDefensorPublico)===0);
  }

  private _filterConductaPunible(valueConductaPunible: string): string[] {
    const filterValueConductaPunible= valueConductaPunible.toLowerCase();
    return this.conductasPunibles.filter(optionConductas => optionConductas.toLowerCase().indexOf(filterValueConductaPunible)===0);
  }

  private _filterResultado(valueResultado: string): string[] {
    const filterValueResultado= valueResultado.toLowerCase();
    return this.resultados.filter(optionResultados => optionResultados.toLowerCase().indexOf(filterValueResultado)===0);
  }

  private _filterPAG(valuePAG: string): string[] {
    const filterValuePAG= valuePAG.toLowerCase();
    return this.pags.filter(optionPAG => optionPAG.toLowerCase().indexOf(filterValuePAG)===0);
  }

  private _filterEstadoProceso(valueEstadoProceso: string): string[] {
    const filterValueEstadoProceso= valueEstadoProceso.toLowerCase();
    return this.estadoProcesos.filter(optionEstadoProcesos => optionEstadoProcesos.toLowerCase().indexOf(filterValueEstadoProceso)===0);
  }

  save(){
    this.misionTrabajoService.getMisionTrabajoList().get().subscribe(response=>{
      this.validarMision = true;
      response.docs.forEach(element => {
        if(this.mision.misionTrabajo.localeCompare(element.data().misionTrabajo)==0){
          this.validarMision = false;
        }
      });
    if(this.validarMision == true){
      this.mision.estadoActivo = true;
      this.mision.estado ="Activo";
      if(this.mision.programa !=undefined && this.mision.ciudadano!=undefined &&
        this.mision.investigacion!=undefined && this.mision.defensorPublico!=undefined && this.mision.pag!=undefined
        && this.mision.estadoProceso!=undefined && this.mision.conductaPunible!=undefined && this.mision.resultado &&
        this.mision.regional!=undefined && this.mision.SPOA!=undefined && this.mision.fechaDeInforme!=undefined &&
        this.mision.fechaDevolucion!=undefined && this.mision.fechaRecibido!=undefined && this.mision.fechaVencimiento!=undefined &&
        this.mision.radicadoInterno!=undefined && this.mision.misionTrabajo!=undefined && this.mision.investigador!=undefined)
        {
          let fecha1 = new Date(this.mision.fechaAsignacion);
          let fecha2 = new Date(this.mision.fechaVencimiento);
          var resta = fecha2.getTime() - fecha1.getTime();
          var resultado = Math.round(resta/ (1000*60*60*24));
          this.mision.diasVencimiento = resultado;  
          this.misionTrabajoService.createMisionTrabajo(this.mision);
          Swal.fire('Se creo la mision de trabajo con exito !! ', "Todo correcto ", 'success');
          this.ciudadanoService.getCiudadanoList().get().subscribe(response=>{
            response.forEach(data=>{
              if(String(data.data().nombre).localeCompare(this.mision.ciudadano)==0){
                this.encontroCiudadano = true;
              }
            });
            if(this.encontroCiudadano==false){
              this.nuevoCiudadano.nombre =this.mision.ciudadano;
              var cedulaNueva = "";
              for (let index = 0; index < 11; index++) {
              cedulaNueva=cedulaNueva+String(Math.round(Math.random() * (10 - 0) + 0));
              }
              this.nuevoCiudadano.cedula = parseInt(cedulaNueva);
              this.ciudadanoService.createCiudadano(this.nuevoCiudadano);
            }
            this.encontroCiudadano=false;
          });
        }else{
          this.tipoError = "Error: Debe de digitar todos los campos";
          Swal.fire('No se creo la mision de trabajo !! ',this.tipoError, 'error');
        }
    }else{
      this.tipoError = "La mision de trabajo con numero "+this.mision.misionTrabajo+" ya existe";
          Swal.fire('No se creo la mision de trabajo !! ',this.tipoError, 'error');
    }
    });
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['/listaMisionTrabajo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

  sacarRegionales(){
    this.regionalService.regionalRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.regionales[contador] = value.nombreRegional;
        contador+=1;
      });
      this.filteredRegional = this.myControlRegional.valueChanges
      .pipe(
        startWith(''),
        map(valueRegional => this._filterRegional(valueRegional))
      );
      
    });
  }

  sacarProgramas(){
    this.programaService.programaRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.programas[contador] = value.nombrePrograma;
        contador+=1;
      });
      this.filteredPrograma = this.myControlPrograma.valueChanges
      .pipe(
        startWith(''),
        map(valuePrograma => this._filterPrograma(valuePrograma))
      );
    });
  }

  sacarCiudadanos(){
    this.ciudadanoService.ciudadanoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.ciudadanos[contador] = value.nombre;
        contador+=1;
      });
      this.filteredCiudadano = this.myControlCiudadano.valueChanges
      .pipe(
        startWith(''),
        map(valueCiudadano => this._filterCiudadano(valueCiudadano))
      );
    });
  }

  sacarDefensoresPublicos(){
    this.defensorPublicoService.defensorPublicoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.defensoresPublicos[contador] = value.nombre;
        contador+=1;
      });
      this.filteredDefensorPublico = this.myControlDefensorPublico.valueChanges
      .pipe(
        startWith(''),
        map(valueDefensorPublico => this._filterDefensorPublico(valueDefensorPublico))
      );
    });
  }


  sacarPags(){
    this.pagService.pagRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.pags[contador] = value.nombre;
        contador+=1;
      });
      this.filteredPAG = this.myControlPAG.valueChanges
      .pipe(
        startWith(''),
        map(valuePag => this._filterPAG(valuePag))
      );
    });
  }

  sacarInvestigaciones(){
    this.investigacionService.investigacionRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.investigaciones[contador] = value.nombreInvestigacion;
        contador+=1;
      });
      this.filteredInvestigacion = this.myControlInvestigacion.valueChanges
      .pipe(
        startWith(''),
        map(valueInvestigacion => this._filterInvestigacion(valueInvestigacion))
      );
    });
  }

  sacarInvestigadores(){
    this.investigadorService.investigadorRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.investigadores[contador] = value.nombre;
        contador+=1;
      });
      this.filteredInvestigador = this.myControlInvestigacion.valueChanges
      .pipe(
        startWith(''),
        map(valueInvestigadores => this._filterInvestigadores(valueInvestigadores))
      );
    });
  }


  sacarEstadoProcesos(){
    this.estadoProcesoService.estadoProcesoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.estadoProcesos[contador] = value.nombreProceso;
        contador+=1;
      });
      this.filteredEstadoProceso = this.myControlEstadoProceso.valueChanges
      .pipe(
        startWith(''),
        map(valueEstadoProceso => this._filterEstadoProceso(valueEstadoProceso))
      );
      
    });
  }

  sacarConductasPunibles(){
    this.conductaPunibleService.conductaPunibleRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.conductasPunibles[contador] = value.nombreConducta;
        contador+=1;
      });
      this.filteredConductaPunible= this.myControlConductaPunible.valueChanges
      .pipe(
        startWith(''),
        map(valueConductas => this._filterConductaPunible(valueConductas))
      );
    });
  }

  sacarResultados(){
    this.resultadoService.resultadoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.resultados[contador] = value.nombreResultado;
        contador+=1;
      });
      this.filteredResultado= this.myControlResultado.valueChanges
      .pipe(
        startWith(''),
        map(valueResultado => this._filterResultado(valueResultado))
      );
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  

}
