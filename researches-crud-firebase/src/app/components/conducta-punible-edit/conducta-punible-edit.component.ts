import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ConductaPunibleService } from 'src/app/services/conducta-punible.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-conducta-punible-edit',
  templateUrl: './conducta-punible-edit.component.html',
  styleUrls: ['./conducta-punible-edit.component.css']
})
export class ConductaPunibleEditComponent implements OnInit {

  public conductaPunible:any;
  public numeroConducta:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public conductaPunibleService:ConductaPunibleService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadUsuario();
  }

  loadUsuario(){
    let params=this.activatedRoute.params['_value'];
    this.numeroConducta = params.conductaPunibleNumero;
    var conductaPunible = {
      nombreConducta:'',
      numeroConducta:0,
    }
    conductaPunible.numeroConducta = params.conductaPunibleNumero;
    this.conductaPunibleService.getConductaPunibleList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroConducta == value.numeroConducta){
          conductaPunible.nombreConducta = value.nombreConducta;
          this.conductaPunible = conductaPunible;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.conductaPunibleService.getConductaPunibleList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroConducta == value.numeroConducta){
          var key = document.id;
          this.conductaPunibleService.updateConductaPunible(key,this.conductaPunible);
          Swal.fire('Se actualizo conducta punible con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
