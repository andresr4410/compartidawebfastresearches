import { Component, OnInit } from '@angular/core';
import { ConductaPunible } from 'src/app/models/conductaPunible';
import { ConductaPunibleService } from 'src/app/services/conducta-punible.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-conducta-punible',
  templateUrl: './guardar-conducta-punible.component.html',
  styleUrls: ['./guardar-conducta-punible.component.css']
})
export class GuardarConductaPunibleComponent implements OnInit {
  conductaPunible:ConductaPunible = new ConductaPunible();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private conductaPunibleService:ConductaPunibleService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.conductaPunibleService.getConductaPunibleList().get().subscribe(response=>{
      var contador = 0;
      var valor =1;
      while(contador<response.docs.length){
        response.forEach(document=>{
          if(valor ==document.data().numeroConducta){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.conductaPunible.numeroConducta = valor;
      if(this.conductaPunible.numeroConducta!=undefined && this.conductaPunible.nombreConducta!=undefined){
        this.conductaPunibleService.createConductaPunible(this.conductaPunible);
        this.conductaPunible = new ConductaPunible();
        Swal.fire('Se creo la conducta punible con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo la conducta punible !! ',this.tipoError, 'error');
      }
    });
  }

  onSubmit(){
    this.save();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    console.log(usuario.idTipoUsuario);
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['listaConducta']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['listaConducta']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
