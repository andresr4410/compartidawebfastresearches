import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ResultadoService } from 'src/app/services/resultado.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-resultado-edit',
  templateUrl: './resultado-edit.component.html',
  styleUrls: ['./resultado-edit.component.css']
})
export class ResultadoEditComponent implements OnInit {

  public resultado:any;
  public numeroResultado:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public resultadoService:ResultadoService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadResultado();
  }

  loadResultado(){
    let params=this.activatedRoute.params['_value'];
    this.numeroResultado = params.resultadoNumero;
    var resultado = {
      nombreResultado:'',
      numeroResultado:0,
      activo:true
    }
    resultado.numeroResultado = params.resultadoNumero;
    this.resultadoService.getResultadoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroResultado == value.numeroResultado){
          resultado.nombreResultado = value.nombreResultado;
          this.resultado = resultado;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.resultadoService.getResultadoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroResultado == value.numeroResultado){
          var key = document.id;
          this.resultadoService.updateResultado(key,this.resultado);
          Swal.fire('Se actualizo resultado con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
