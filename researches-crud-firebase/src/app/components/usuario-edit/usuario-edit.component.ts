import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-usuario-edit',
  templateUrl: './usuario-edit.component.html',
  styleUrls: ['./usuario-edit.component.css']
})
export class UsuarioEditComponent implements OnInit {
  public usuario:any;
  public idUsuario:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public usuarioService:UsuarioService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadUsuario();
  }

  loadUsuario(){
    let params=this.activatedRoute.params['_value'];
    this.idUsuario = params.usuarioId;
    var usuario = {
      email:'',
      clave:'',
      idUsuario:0,
      idTipoUsuario:0,
      activo:true
    }
    usuario.idUsuario = parseInt(params.usuarioId);
    this.usuarioService.getUsuarioList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.idUsuario == value.idUsuario){
          usuario.email = value.email;
          usuario.clave = value.clave;
          usuario.idTipoUsuario = value.idTipoUsuario;
          usuario.activo = value.activo;
          this.usuario = usuario;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.usuarioService.getUsuarioList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.idUsuario == value.idUsuario){
          var key = document.id;
          this.usuarioService.updateUsuario(key,this.usuario);
          Swal.fire('Se actualizo usuario con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
