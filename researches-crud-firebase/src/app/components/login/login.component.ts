import { Component, OnInit } from '@angular/core';
import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  title = 'Defensoria del Pueblo';
  emailAddress: string;
  password: string;
  dato:any;
  error = 'Correo o contraseña incorrectos! ';

   ngOnInit() {
  }
  constructor(public authService: AuthService,
    public usuarioService:UsuarioService) { }
 /*
  signup() {

    this.authService.signup(this.emailAddress, this.password);
    this.emailAddress = this.password = '';
  }
  */
 
  login() {
      this.authService.login(this.emailAddress, this.password);
      this.emailAddress = this.password = '';
  }
 
  logout() {
    this.authService.logout();
    this.emailAddress = this.password = '';
  }
}
