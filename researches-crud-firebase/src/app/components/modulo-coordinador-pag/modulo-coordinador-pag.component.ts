import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modulo-coordinador-pag',
  templateUrl: './modulo-coordinador-pag.component.html',
  styleUrls: ['./modulo-coordinador-pag.component.css']
})
export class ModuloCoordinadorPagComponent implements OnInit {
  sideBarOpen = true;
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  constructor(public authService:AuthService,
    public router:Router) { }


  ngOnInit(): void {
  }
  toggleSideBar(){
    this.sideBarOpen = !this.sideBarOpen;
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
}
