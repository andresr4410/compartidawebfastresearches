import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstadoProcesoService } from 'src/app/services/estado-proceso.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-estado-proceso-edit',
  templateUrl: './estado-proceso-edit.component.html',
  styleUrls: ['./estado-proceso-edit.component.css']
})
export class EstadoProcesoEditComponent implements OnInit {


  public estadoProceso:any;
  public numeroProceso:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public estadoProcesoService:EstadoProcesoService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadEstadoProceso();
  }

  loadEstadoProceso(){
    let params=this.activatedRoute.params['_value'];
    this.numeroProceso = params.estadoProcesoNumero;
    var estadoProceso = {
      nombreProceso:'',
      numeroProceso:0,
    }
    estadoProceso.numeroProceso = params.estadoProcesoNumero;
    this.estadoProcesoService.getEstadoProcesoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroProceso == value.numeroProceso){
          estadoProceso.nombreProceso = value.nombreProceso;
          this.estadoProceso = estadoProceso;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.estadoProcesoService.getEstadoProcesoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroProceso == value.numeroProceso){
          var key = document.id;
          this.estadoProcesoService.updateEstadoProceso(key,this.estadoProceso);
          Swal.fire('Se actualizo estado de proceso con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
