import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ConductaPunible } from 'src/app/models/conductaPunible';
import { MatPaginator } from '@angular/material/paginator';
import { ConductaPunibleService } from 'src/app/services/conducta-punible.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-conducta-punible-list',
  templateUrl: './conducta-punible-list.component.html',
  styleUrls: ['./conducta-punible-list.component.css']
})
export class ConductaPunibleListComponent implements OnInit {

  displayedColums:string[] = ['numeroConducta','nombreConducta','opciones'];
  conductasPunibles:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<ConductaPunible>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public conductaPunibleService:ConductaPunibleService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.conductaPunibleService.getConductaPunibleList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(conductasPunibles=>{
      this.conductasPunibles =conductasPunibles;
      this.dataSource=new MatTableDataSource(this.conductasPunibles);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(numeroConducta:string,nombreConducta:string){
    this.conductaPunibleService.getConductaPunibleList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(numeroConducta == value.numeroConducta){
          var key = document.id;
          this.conductaPunibleService.deleteConductaPunible(key);
          Swal.fire('Se elimino correctamente !! ', "Todo correcto ", 'success');
          this.nombre =nombreConducta ;
        }
      });
    });
  }

  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
