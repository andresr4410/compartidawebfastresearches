import { Component, OnInit } from '@angular/core';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { DefensorPublico } from 'src/app/models/DefensorPublico';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-defensor',
  templateUrl: './guardar-defensor.component.html',
  styleUrls: ['./guardar-defensor.component.css']
})
export class GuardarDefensorComponent implements OnInit {
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public defensor:DefensorPublico = new DefensorPublico();
  public tipoError:string = "";

  constructor(private defensorPublicoService:DefensorPublicoService,
    public router:Router,
    public authService:AuthService) { }
  

  ngOnInit(): void {

  }
  onSubmit(){
    this.save();
  }
  save(){
    if(this.defensor.nombre !=undefined && this.defensor.email!=undefined &&
      this.defensor.apellido!=undefined && this.defensor.cedula!=undefined && this.defensor.arl!=undefined
      && this.defensor.eps!=undefined && this.defensor.contrato!=undefined && this.defensor.pension &&
      this.defensor.telefono!=undefined){
        this.defensorPublicoService.createDefensorPublico(this.defensor);
        Swal.fire('Se creo el defensor publico con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el defensor publico !! ',this.tipoError, 'error');
      }
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }


}
