import { Component, OnInit, ViewChild } from '@angular/core';
import { Investigador } from 'src/app/models/investigador';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-investigador-list',
  templateUrl: './investigador-list.component.html',
  styleUrls: ['./investigador-list.component.css']
})
export class InvestigadorListComponent implements OnInit {

  displayedColums:string[] = ['cedula','nombre','apellido','correo','cargo','direccion','telefono','opciones'];
  investigadores:any;
  public correcto:boolean=false;
  public cedula:string="";

  dataSource:MatTableDataSource<Investigador>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public investigadorService:InvestigadorService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.investigadorService.getInvestigadorList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(investigadores=>{
      this.investigadores = investigadores;
      this.dataSource=new MatTableDataSource(this.investigadores);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(cedula:string){
    this.investigadorService.getInvestigadorList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(cedula == value.cedula){
          var key = document.id;
          this.investigadorService.deleteInvestigador(key);
          Swal.fire('Se elimino correctamente !! ', "Todo correcto ", 'success');
          this.cedula = cedula;
        }
      });
    });
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
