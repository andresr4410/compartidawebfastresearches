import { Component, OnInit } from '@angular/core';
import { TipoUsuario } from 'src/app/models/tipoUsuario';
import { TipoUsuarioService } from 'src/app/services/tipo-usuario.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-tipo-usuario',
  templateUrl: './guardar-tipo-usuario.component.html',
  styleUrls: ['./guardar-tipo-usuario.component.css']
})
export class GuardarTipoUsuarioComponent implements OnInit {

  tipoUsuario:TipoUsuario = new TipoUsuario();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private tipoUsuarioService:TipoUsuarioService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.tipoUsuarioService.getTipoUsuarioList().get().subscribe(response=>{
      var contador = 0;
      var valor =1;
      while(contador<response.docs.length){
        response.forEach(document=>{
          if(valor ==document.data().idTipoUsuario){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.tipoUsuario.idTipoUsuario = valor;
      if(this.tipoUsuario.idTipoUsuario!=undefined && this.tipoUsuario.rol!=undefined){
        this.tipoUsuarioService.createTipoUsuario(this.tipoUsuario);
        this.tipoUsuario = new TipoUsuario();
        Swal.fire('Se creo el tipo de usuario con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el tipo de usuario !! ',this.tipoError, 'error');
      }
    });
  }

  onSubmit(){
    this.save();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    console.log(usuario.idTipoUsuario);
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
