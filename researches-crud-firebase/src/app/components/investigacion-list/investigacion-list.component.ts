import { Component, OnInit, ViewChild } from '@angular/core';
import { Investigacion } from 'src/app/models/investigacion';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { InvestigacionService } from 'src/app/services/investigacion.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { MatSort } from '@angular/material/sort';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-investigacion-list',
  templateUrl: './investigacion-list.component.html',
  styleUrls: ['./investigacion-list.component.css']
})
export class InvestigacionListComponent implements OnInit {

  displayedColums:string[] = ['numeroInvestigacion','nombreInvestigacion','opciones'];
  investigaciones:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<Investigacion>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public investigacionService:InvestigacionService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.investigacionService.getInvestigacionList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(investigaciones=>{
      this.investigaciones =investigaciones;
      this.dataSource=new MatTableDataSource(this.investigaciones);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(numeroInvestigacion:string,nombreInvestigacion:string){
    this.investigacionService.getInvestigacionList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(numeroInvestigacion == value.numeroInvestigacion){
          var key = document.id;
          this.investigacionService.deleteInvestigacion(key);
          Swal.fire('Se elimino correctamente !! ', "Todo correcto ", 'success');
          this.nombre =nombreInvestigacion ;
        }
      });
    });
  }

  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
