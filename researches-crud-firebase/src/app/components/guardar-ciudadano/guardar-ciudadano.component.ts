import { Component, OnInit } from '@angular/core';
import { Ciudadano } from 'src/app/models/ciudadano';
import { CiudadanoService } from 'src/app/services/ciudadano.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-ciudadano',
  templateUrl: './guardar-ciudadano.component.html',
  styleUrls: ['./guardar-ciudadano.component.css']
})
export class GuardarCiudadanoComponent implements OnInit {

  ciudadano:Ciudadano = new Ciudadano();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  tipoError = "";

  constructor(private ciudadanoService:CiudadanoService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    if(this.ciudadano.nombre !=undefined && this.ciudadano.edad !=undefined &&
      this.ciudadano.apellido!=undefined && this.ciudadano.cedula!=undefined && this.ciudadano.ciudad!=undefined
      && this.ciudadano.direccion!=undefined && this.ciudadano.email!=undefined &&
      this.ciudadano.telefono!=undefined){
        this.ciudadanoService.createCiudadano(this.ciudadano);
        Swal.fire('Se creo el ciudadano con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el ciudadano !! ',this.tipoError, 'error');
      } 
  }

  onSubmit(){
    this.save();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['/listaCiudadano']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['listaCiudadano']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['listaCiudadano'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['listaCiudadano']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['listaCiudadano']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['listaCiudadano']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['listaCiudadano']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }


}
