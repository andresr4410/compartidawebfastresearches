import { Component, OnInit } from '@angular/core';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-defensor-publico-edit',
  templateUrl: './defensor-publico-edit.component.html',
  styleUrls: ['./defensor-publico-edit.component.css']
})
export class DefensorPublicoEditComponent implements OnInit {
  public defensorPublico:any;
  public cedula:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public defensorPublicoService:DefensorPublicoService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadDefensor();
  }

  loadDefensor(){
    let params=this.activatedRoute.params['_value'];
    this.cedula = params.cedulaDefensor;
    var defensorPublico = {
      nombre:'',
      apellido:'',
      arl:'',
      eps:'',
      contrato:'',
      telefono:0,
      pension:'',
      direccion:'',
      email:'',
      cedula:''
    }
    defensorPublico.cedula = params.cedulaDefensor;
    this.defensorPublicoService.getDefensorPublicoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          defensorPublico.nombre = value.nombre;
          defensorPublico.apellido = value.apellido;
          defensorPublico.arl = value.arl;
          defensorPublico.eps = value.eps;
          defensorPublico.contrato = value.contrato;
          defensorPublico.telefono = value.telefono;
          defensorPublico.pension = value.pension;
          defensorPublico.direccion = value.direccion;
          defensorPublico.email = value.email;
          this.defensorPublico = defensorPublico;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.defensorPublicoService.getDefensorPublicoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          var key = document.id;
          this.defensorPublicoService.updateDefensorPublico(key,this.defensorPublico);
          Swal.fire('Se actualizo defensor publico con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }
}
