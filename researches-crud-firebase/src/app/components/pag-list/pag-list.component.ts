import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PAG } from 'src/app/models/pag';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PagService } from 'src/app/services/pag.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-pag-list',
  templateUrl: './pag-list.component.html',
  styleUrls: ['./pag-list.component.css']
})
export class PagListComponent implements OnInit {
 
  displayedColums:string[] = ['cedula','nombre','apellido','direccion','email','telefono','opciones'];
  pags:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<PAG>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public pagService:PagService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.pagService.getPagList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(pags=>{
      this.pags =pags;
      this.dataSource=new MatTableDataSource(this.pags);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(idPag:string,nombre:string){
    this.pagService.getPagList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(idPag == value.idPag){
          var key = document.id;
          this.pagService.deletePag(key);
          Swal.fire('Se Elimino con exito !! ', "Todo correcto ", 'success');
          this.nombre =nombre ;
        }
      });
    });
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
