import { Component, OnInit } from '@angular/core';
import { Programa } from 'src/app/models/programa';
import { ProgramaService } from 'src/app/services/programa.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-programa',
  templateUrl: './guardar-programa.component.html',
  styleUrls: ['./guardar-programa.component.css']
})
export class GuardarProgramaComponent implements OnInit {

  programa:Programa = new Programa();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private programaService:ProgramaService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.programaService.getProgramaList().get().subscribe(response=>{
      var contador = 0;
      var valor =1;
      while(contador<response.docs.length){
        response.forEach(document=>{
          if(valor ==document.data().numeroPrograma){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.programa.numeroPrograma = valor;
      if(this.programa.numeroPrograma!=undefined && this.programa.nombrePrograma!=undefined){
        this.programaService.createPrograma(this.programa);
        this.programa = new Programa();
        Swal.fire('Se creo el programa con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el programa !! ',this.tipoError, 'error');
      }
    });
  }

  onSubmit(){
    this.save();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
}
