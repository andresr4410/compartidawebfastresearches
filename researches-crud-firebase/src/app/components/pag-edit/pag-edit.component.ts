import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PagService } from 'src/app/services/pag.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-pag-edit',
  templateUrl: './pag-edit.component.html',
  styleUrls: ['./pag-edit.component.css']
})
export class PagEditComponent implements OnInit {
  public pag:any;
  public cedula:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public pagService:PagService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadPag();
  }

  loadPag(){
    let params=this.activatedRoute.params['_value'];
    this.cedula = params.cedulaPag;
    var pag = {
      nombre:'',
      apellido:'',
      telefono:0,
      direccion:'',
      email:'',
      cedula:''
    }
    pag.cedula = params.cedulaPag;
    this.pagService.getPagList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          pag.nombre = value.nombre;
          pag.apellido = value.apellido;
          pag.telefono = value.telefono;
          pag.direccion = value.direccion;
          pag.email = value.email;
          this.pag = pag;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.pagService.getPagList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          var key = document.id;
          this.pagService.updatePag(key,this.pag);
          Swal.fire('Se actualizo PAG con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }
}
