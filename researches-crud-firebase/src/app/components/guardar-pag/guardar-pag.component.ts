import { Component, OnInit } from '@angular/core';
import { PAG } from 'src/app/models/pag';
import { PagService } from 'src/app/services/pag.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-pag',
  templateUrl: './guardar-pag.component.html',
  styleUrls: ['./guardar-pag.component.css']
})
export class GuardarPAGComponent implements OnInit {
  public correcto:boolean=false;
  public pag:PAG = new PAG();
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private pagService:PagService,
    public router:Router,
    public authService:AuthService) { }
  

  ngOnInit(): void {

  }
  onSubmit(){
    this.save();
  }
  save(){
    if(this.pag.nombre !=undefined &&
      this.pag.apellido!=undefined && this.pag.cedula!=undefined 
      && this.pag.direccion!=undefined && this.pag.email!=undefined &&
      this.pag.telefono!=undefined){
        this.pagService.createPag(this.pag);
        Swal.fire('Se creo el PAG con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el PAG !! ',this.tipoError, 'error');
      } 
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    console.log(usuario.idTipoUsuario);
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
