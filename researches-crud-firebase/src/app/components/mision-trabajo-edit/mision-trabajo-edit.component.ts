import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { RegionalService } from 'src/app/services/regional.service';
import { ResultadoService } from 'src/app/services/resultado.service';
import { ConductaPunibleService } from 'src/app/services/conducta-punible.service';
import { EstadoProcesoService } from 'src/app/services/estado-proceso.service';
import { PagService } from 'src/app/services/pag.service';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { InvestigacionService } from 'src/app/services/investigacion.service';
import { CiudadanoService } from 'src/app/services/ciudadano.service';
import { ProgramaService } from 'src/app/services/programa.service';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import { map, startWith } from 'rxjs/operators';
@Component({
  selector: 'app-mision-trabajo-edit',
  templateUrl: './mision-trabajo-edit.component.html',
  styleUrls: ['./mision-trabajo-edit.component.css']
})
export class MisionTrabajoEditComponent implements OnInit {
  public mision:any;
  public radicadoInterno:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  public programas:string[];
  public ciudadanos:string[];
  public investigaciones:string[];
  public investigadores:string[];
  public defensoresPublicos:string[];
  public pags:string[];
  public regionales:string[];
  public estadoProcesos:string[];
  public conductasPunibles:string[];
  public resultados:string[];

  filteredRegional: Observable<string[]>;
  filteredPrograma: Observable<string[]>;
  filteredCiudadano: Observable<string[]>;
  filteredInvestigador: Observable<string[]>;
  filteredInvestigacion: Observable<string[]>;
  filteredDefensorPublico:Observable<string[]>;
  filteredResultado:Observable<string[]>;
  filteredPAG:Observable<string[]>;
  filteredEstadoProceso:Observable<string[]>;
  filteredConductaPunible:Observable<string[]>;
  myControlRegional: FormControl;
  myControlPrograma:FormControl;
  myControlCiudadano:FormControl;
  myControlInvestigador:FormControl;
  myControlInvestigacion:FormControl;
  myControlDefensorPublico:FormControl;
  myControlResultado:FormControl;
  myControlPAG:FormControl;
  myControlEstadoProceso:FormControl;
  myControlConductaPunible:FormControl;
  constructor(public activatedRoute:ActivatedRoute,
    public misionTrabajoService:MisionTrabajoService,
    public authService:AuthService,
    public router:Router,
    public regionalService:RegionalService,
    public resultadoService:ResultadoService,
    public conductaPunibleService:ConductaPunibleService,
    public estadoProcesoService:EstadoProcesoService,
    public pagService:PagService,
    public defensorPublicoService:DefensorPublicoService,
    public investigacionService:InvestigacionService,
    public ciudadanoService:CiudadanoService,
    public programaService:ProgramaService,
    public investigadorService:InvestigadorService,
    public misionFilesService:MisionFileServicesService) { }

  ngOnInit(): void {
    this.loadMision();
    this.myControlPrograma = new FormControl('');
    this.myControlRegional = new FormControl('');
    this.myControlCiudadano = new FormControl('');
    this.myControlInvestigador = new FormControl('');
    this.myControlInvestigacion = new FormControl('');
    this.myControlDefensorPublico = new FormControl('');
    this.myControlConductaPunible = new FormControl('');
    this.myControlResultado = new FormControl('');
    this.myControlPAG = new FormControl('');
    this.myControlEstadoProceso = new FormControl('');
    this.programas = ['',''];
    this.sacarProgramas();
    this.ciudadanos = ['',''];
    this.sacarCiudadanos();
    this.regionales = ['',''];
    this.sacarRegionales();
    this.defensoresPublicos = ['',''];
    this.sacarDefensoresPublicos();
    this.pags = ['',''];
    this.sacarPags();
    this.investigaciones= ['',''];
    this.sacarInvestigaciones();
    this.regionales = ['',''];
    this.sacarRegionales();
    this.estadoProcesos = ['',''];
    this.sacarEstadoProcesos ();
    this.conductasPunibles = ['',''];
    this.sacarConductasPunibles();
    this.resultados = ['',''];
    this.sacarResultados();   
    this.investigadores = ['',''];
    this.sacarInvestigadores(); 
  }

  sacarRegionales(){
    this.regionalService.regionalRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.regionales[contador] = value.nombreRegional;
        contador+=1;
      });
      this.filteredRegional = this.myControlRegional.valueChanges
      .pipe(
        startWith(''),
        map(valueRegional => this._filterRegional(valueRegional))
      );
      
    });
  }

  sacarProgramas(){
    this.programaService.programaRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.programas[contador] = value.nombrePrograma;
        contador+=1;
      });
      this.filteredPrograma = this.myControlPrograma.valueChanges
      .pipe(
        startWith(''),
        map(valuePrograma => this._filterPrograma(valuePrograma))
      );
    });
  }

  sacarCiudadanos(){
    this.ciudadanoService.ciudadanoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.ciudadanos[contador] = value.nombre;
        contador+=1;
      });
      this.filteredCiudadano = this.myControlCiudadano.valueChanges
      .pipe(
        startWith(''),
        map(valueCiudadano => this._filterCiudadano(valueCiudadano))
      );
    });
  }

  sacarDefensoresPublicos(){
    this.defensorPublicoService.defensorPublicoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.defensoresPublicos[contador] = value.nombre;
        contador+=1;
      });
      this.filteredDefensorPublico = this.myControlDefensorPublico.valueChanges
      .pipe(
        startWith(''),
        map(valueDefensorPublico => this._filterDefensorPublico(valueDefensorPublico))
      );
    });
  }


  sacarPags(){
    this.pagService.pagRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.pags[contador] = value.nombre;
        contador+=1;
      });
      this.filteredPAG = this.myControlPAG.valueChanges
      .pipe(
        startWith(''),
        map(valuePag => this._filterPAG(valuePag))
      );
    });
  }

  sacarInvestigaciones(){
    this.investigacionService.investigacionRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.investigaciones[contador] = value.nombreInvestigacion;
        contador+=1;
      });
      this.filteredInvestigacion = this.myControlInvestigacion.valueChanges
      .pipe(
        startWith(''),
        map(valueInvestigacion => this._filterInvestigacion(valueInvestigacion))
      );
    });
  }

  sacarInvestigadores(){
    this.investigadorService.investigadorRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.investigadores[contador] = value.nombre;
        contador+=1;
      });
      this.filteredInvestigador = this.myControlInvestigacion.valueChanges
      .pipe(
        startWith(''),
        map(valueInvestigadores => this._filterInvestigadores(valueInvestigadores))
      );
    });
  }


  sacarEstadoProcesos(){
    this.estadoProcesoService.estadoProcesoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.estadoProcesos[contador] = value.nombreProceso;
        contador+=1;
      });
      this.filteredEstadoProceso = this.myControlEstadoProceso.valueChanges
      .pipe(
        startWith(''),
        map(valueEstadoProceso => this._filterEstadoProceso(valueEstadoProceso))
      );
      
    });
  }

  sacarConductasPunibles(){
    this.conductaPunibleService.conductaPunibleRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.conductasPunibles[contador] = value.nombreConducta;
        contador+=1;
      });
      this.filteredConductaPunible= this.myControlConductaPunible.valueChanges
      .pipe(
        startWith(''),
        map(valueConductas => this._filterConductaPunible(valueConductas))
      );
    });
  }

  sacarResultados(){
    this.resultadoService.resultadoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.resultados[contador] = value.nombreResultado;
        contador+=1;
      });
      this.filteredResultado= this.myControlResultado.valueChanges
      .pipe(
        startWith(''),
        map(valueResultado => this._filterResultado(valueResultado))
      );
    });
  }
  loadMision(){
    let params=this.activatedRoute.params['_value'];
    this.radicadoInterno = params.numeroRadicado;
    var mision = {
      radicadoInterno:'',
      misionTrabajo:'',
      programa:'',
      ciudadano:'',
      defensorPublico:'',
      regional:'',
      pag:'',
      estadoProceso:'',
      resultado:'',
      investigacion:'',
      investigador:'',
      conductaPunible:''
    }
    mision.radicadoInterno = this.radicadoInterno;
    this.misionTrabajoService.getMisionTrabajoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.radicadoInterno.localeCompare(value.radicadoInterno)==0){
          mision.radicadoInterno = value.radicadoInterno;
          mision.misionTrabajo = value.misionTrabajo;
          mision.programa = value.programa;
          mision.ciudadano = value.ciudadano;
          mision.defensorPublico = value.defensorPublico;
          mision.regional = value.regional;
          mision.pag = value.pag;
          mision.estadoProceso = value.estadoProceso;
          mision.resultado = value.resultado;
          mision.investigacion = value.investigacion;
          mision.investigador = value.investigador;
          mision.conductaPunible = value.conductaPunible;
          this.mision = mision;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.misionTrabajoService.getMisionTrabajoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.radicadoInterno.localeCompare(value.radicadoInterno)==0){
          var key = document.id;
          this.misionTrabajoService.updateMisionTrabajo(key,this.mision);
          Swal.fire('Se actualizo mision de trabajo con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }


  
  private _filterRegional(valueRegional: string): string[] {
    const filterValueRegional = valueRegional.toLowerCase();
    return this.regionales.filter(optionRegional => optionRegional.toLowerCase().indexOf(filterValueRegional)===0);
  }

  private _filterPrograma(valuePrograma: string): string[] {
    const filterValuePrograma = valuePrograma.toLowerCase();
    return this.programas.filter(optionPrograma => optionPrograma.toLowerCase().indexOf(filterValuePrograma)===0);
  }

  private _filterCiudadano(valueCiudadano: string): string[] {
    const filterValueCiudadano = valueCiudadano.toLowerCase();
    return this.ciudadanos.filter(optionCiudadano => optionCiudadano.toLowerCase().indexOf(filterValueCiudadano)===0);
  }

  private _filterInvestigadores(valueInvestigador: string): string[] {
    const filterValueInvestigador = valueInvestigador.toLowerCase();
    return this.investigadores.filter(optionInvestigador => optionInvestigador.toLowerCase().indexOf(filterValueInvestigador)===0);
  }

  private _filterInvestigacion(valueInvestigacion: string): string[] {
    const filterValueInvestigacion = valueInvestigacion.toLowerCase();
    return this.investigaciones.filter(optionInvestigacion => optionInvestigacion.toLowerCase().indexOf(filterValueInvestigacion)===0);
  }

  private _filterDefensorPublico(valueDefensorPublico: string): string[] {
    const filterValueDefensorPublico = valueDefensorPublico.toLowerCase();
    return this.defensoresPublicos.filter(OptionDefensorPublico => OptionDefensorPublico.toLowerCase().indexOf(filterValueDefensorPublico)===0);
  }

  private _filterConductaPunible(valueConductaPunible: string): string[] {
    const filterValueConductaPunible= valueConductaPunible.toLowerCase();
    return this.conductasPunibles.filter(optionConductas => optionConductas.toLowerCase().indexOf(filterValueConductaPunible)===0);
  }

  private _filterResultado(valueResultado: string): string[] {
    const filterValueResultado= valueResultado.toLowerCase();
    return this.resultados.filter(optionResultados => optionResultados.toLowerCase().indexOf(filterValueResultado)===0);
  }

  private _filterPAG(valuePAG: string): string[] {
    const filterValuePAG= valuePAG.toLowerCase();
    return this.pags.filter(optionPAG => optionPAG.toLowerCase().indexOf(filterValuePAG)===0);
  }

  private _filterEstadoProceso(valueEstadoProceso: string): string[] {
    const filterValueEstadoProceso= valueEstadoProceso.toLowerCase();
    return this.estadoProcesos.filter(optionEstadoProcesos => optionEstadoProcesos.toLowerCase().indexOf(filterValueEstadoProceso)===0);
  }
}
