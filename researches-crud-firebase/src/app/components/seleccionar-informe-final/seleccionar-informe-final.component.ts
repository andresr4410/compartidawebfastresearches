import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { AuthService } from 'src/app/services/auth.service';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';

@Component({
  selector: 'app-seleccionar-informe-final',
  templateUrl: './seleccionar-informe-final.component.html',
  styleUrls: ['./seleccionar-informe-final.component.css']
})
export class SeleccionarInformeFinalComponent implements OnInit {

  displayedColums:string[] = ['misionTrabajo','investigacion','investigador','seleccionarEvidencias'];
  misionTrabajos:any;
  diasVencimiento:number = 0;

  dataSource:MatTableDataSource<MisionTrabajo>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public misionTrabajoService:MisionTrabajoService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router,
    public activatedRoute:ActivatedRoute) { 
    
    }

  ngOnInit(): void {
    this.inicializarLista();
  }


  inicializarLista(){
    this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(misionTrabajos=>{
      var arregloValores = new Array();
      misionTrabajos.forEach(data=>{
        var fechaActual = new Date();
        let fecha1 = new Date(fechaActual);
        let fecha2 = new Date(data.fechaVencimiento);
        let resta = fecha2.getTime() - fecha1.getTime();
        var resultado = Math.round(resta/ (1000*60*60*24));
        data.diasVencimiento = resultado;
      }); 
      var resultado = JSON.parse(localStorage.getItem(('usuario')));
      if(resultado.idTipoUsuario == 2 ){
        this.dataSource=new MatTableDataSource(misionTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      }else{
        misionTrabajos.forEach(datosMision=>{
          if(datosMision.estado.localeCompare("Activo")!=0){
            arregloValores.push(misionTrabajos.indexOf(datosMision));
          }
        });
        var contador =0;
        for( var i = 0; i < arregloValores.length; i++){
          misionTrabajos.splice(arregloValores[contador], 1);
          for (let index = 0; index < arregloValores.length; index++) {
            arregloValores[index] = arregloValores[index] - 1 ;
          }
          contador  = contador + 1 ;
        }
        this.dataSource=new MatTableDataSource(misionTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      }
    });
  }

  verEvidenciasMision(idx:number){
    this.router.navigate(['/informeFinal',idx]);
  }

  getColor(inUtil) {
    if (inUtil <0)
        return 'red';
    else
        return 'green';
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['/moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
