import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { InformeFinalService } from 'src/app/services/informe-final.service';

@Component({
  selector: 'app-revisar-evidencia-informe-final',
  templateUrl: './revisar-evidencia-informe-final.component.html',
  styleUrls: ['./revisar-evidencia-informe-final.component.css']
})
export class RevisarEvidenciaInformeFinalComponent implements OnInit {
  itemsFinalesEvidencias:any[]=[];
  paramIdMision:any;
  constructor(public router:Router,
    public authService:AuthService, public informeFinalService:InformeFinalService, public activatedRoute:ActivatedRoute) { 
    this.paramIdMision=this.activatedRoute.params['_value'];
    this.informeFinalService.getInformeFinalList().get().subscribe(response=>{
      response.forEach(data=>{
        var element = data.data();
        if(element.idMision == this.paramIdMision.idMision){
          this.itemsFinalesEvidencias.push(element);
        }
      });
    });
  }

  ngOnInit(): void {
  }





  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

    
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
