import { Component, OnInit } from '@angular/core';
import { EstadoProceso } from 'src/app/models/estadoProceso';
import { EstadoProcesoService } from 'src/app/services/estado-proceso.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-estado-proceso',
  templateUrl: './guardar-estado-proceso.component.html',
  styleUrls: ['./guardar-estado-proceso.component.css']
})
export class GuardarEstadoProcesoComponent implements OnInit {
  estadoProceso:EstadoProceso = new EstadoProceso();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private estadoProcesoService:EstadoProcesoService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.estadoProcesoService.getEstadoProcesoList().get().subscribe(response=>{
      var contador = 0;
      var valor =1;
      while(contador<response.docs.length){
        response.forEach(document=>{
          if(valor ==document.data().numeroProceso){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.estadoProceso.numeroProceso = valor;
      if(this.estadoProceso.numeroProceso!=undefined && this.estadoProceso.nombreProceso!=undefined){
        this.estadoProcesoService.createEstadoProceso(this.estadoProceso);
        this.estadoProceso = new EstadoProceso();
        Swal.fire('Se creo el estado de proceso con exito !! ', "Todo correcto ", 'success');
      }else{
        Swal.fire('No se creo el estado de proceso !! ',this.tipoError, 'error');
      }
    });
  }

  onSubmit(){
    this.save();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    console.log(usuario.idTipoUsuario);
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
