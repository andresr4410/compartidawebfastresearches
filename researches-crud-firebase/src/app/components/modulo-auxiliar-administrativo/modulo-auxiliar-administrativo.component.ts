import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { DetailsUploadComponent } from '../upload/details-upload/details-upload.component';

@Component({
  selector: 'app-modulo-auxiliar-administrativo',
  templateUrl: './modulo-auxiliar-administrativo.component.html',
  styleUrls: ['./modulo-auxiliar-administrativo.component.css']
})
export class ModuloAuxiliarAdministrativoComponent implements OnInit {
  sideBarOpen = true;
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  constructor(public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  toggleSideBar(){
    this.sideBarOpen = !this.sideBarOpen;
  }
}
