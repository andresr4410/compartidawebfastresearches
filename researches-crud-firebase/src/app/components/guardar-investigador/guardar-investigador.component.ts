import { Component, OnInit } from '@angular/core';
import { Investigador } from 'src/app/models/investigador';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-investigador',
  templateUrl: './guardar-investigador.component.html',
  styleUrls: ['./guardar-investigador.component.css']
})
export class GuardarInvestigadorComponent implements OnInit {

  public correcto:boolean=false;
  public investigador:Investigador = new Investigador();
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private investigadorService:InvestigadorService,
    public router:Router,
    public authService:AuthService) { }
  

  ngOnInit(): void {

  }
  onSubmit(){
    this.save();
  }
  save(){
    if(this.investigador.nombre !=undefined && this.investigador.cargo!=undefined &&
      this.investigador.apellido!=undefined && this.investigador.cedula!=undefined && this.investigador.cargo!=undefined
      && this.investigador.direccion!=undefined && this.investigador.email!=undefined &&
      this.investigador.telefono!=undefined){
        this.investigadorService.createInvestigador(this.investigador);
        Swal.fire('Se creo el investigador con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el investigador !! ',this.tipoError, 'error');
      }
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
