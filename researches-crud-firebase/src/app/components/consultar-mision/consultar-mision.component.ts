import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-consultar-mision',
  templateUrl: './consultar-mision.component.html',
  styleUrls: ['./consultar-mision.component.css']
})
export class ConsultarMisionComponent implements OnInit {
  misionTrabajo:number;
  incorrecto:boolean = false;
  activarDescarga = false;
  mision:MisionTrabajo = new MisionTrabajo();
  correcto:boolean = false;
  url:string="";
  name:string="";
  constructor(public authService:AuthService,
    public router:Router,
    public misionTrabajoService:MisionTrabajoService,
    public misionTrabajoFilesService:MisionFileServicesService) { }

  ngOnInit(): void {
    
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag']);
    }
  }

  consultar(){
      this.misionTrabajoService.misionTrabajoRef.get().subscribe(response=>{
        if(this.misionTrabajo==undefined){
          Swal.fire('No se pudo consultar !! ',"No digito ningun radicado interno ", 'error');
        }else if(this.misionTrabajo!=undefined){
          this.correcto = false;
        response.forEach(data=>{
          const value = data.data();
          if(this.misionTrabajo == value.misionTrabajo){
            this.mision.radicadoInterno = value.radicadoInterno;
            this.mision.SPOA = value.SPOA;
            this.mision.fechaDeInforme = value.fechaDeInforme;
            this.mision.fechaRecibido = value.fechaRecibido;
            this.mision.fechaDevolucion = value.fechaDevolucion;
            this.mision.fechaVencimiento = value.fechaVencimiento;
            this.mision.fechaAsignacion = value.fechaAsignacion;
            this.mision.regional = value.regional;
            this.mision.investigacion = value.investigacion;
            this.mision.investigador = value.investigador;
            this.mision.numeroInforme = value.numeroInforme;
            this.mision.pag = value.pag;
            this.mision.ciudadano = value.ciudadano;
            this.mision.observaciones = value.observaciones;
            this.mision.resultado = value.resultado;
            this.mision.estadoProceso = value.estadoProceso;
            this.mision.misionTrabajo = value.misionTrabajo;
            this.mision.programa = value.programa;
            this.mision.defensorPublico = value.defensorPublico;
            this.mision.conductaPunible = value.conductaPunible;
            this.correcto = true;
          }
        });
        var comprobar =String(this.misionTrabajo);
        var radicadoInterno ="";
        var contador = 0;
        do{
          radicadoInterno+=comprobar[contador];
          contador+=1;
        }while(comprobar[contador]!='-');
        console.log(radicadoInterno);
        this.activarDescarga = false;
        var radicadoString1 = String(radicadoInterno+".docx");
        var radicadoString2 = String(radicadoInterno+".pptx");
        var radicadoString3 = String(radicadoInterno+".pdf");
        var radicadoString4 = String(radicadoInterno+".rar");
        var radicadoString5 = String(radicadoInterno+".zip");
        this.misionTrabajoFilesService.getUploadList().get().subscribe(response=>{
          response.docs.forEach(data=>{
            const value =data.data();
            if(radicadoString1.localeCompare(value.name)==0 || radicadoString2.localeCompare(value.name)==0
            || radicadoString3.localeCompare(value.name)==0 || radicadoString4.localeCompare(value.name)==0
            || radicadoString5.localeCompare(value.name)==0){
              this.url = value.url;
              this.name = value.name;
              this.activarDescarga = true;
            }
          });
        });
      }
      if(this.mision.ciudadano==undefined){
        Swal.fire('No se pudo consultar !! ',"No digito ninguna mision de trabajo ", 'error');
      }

      if(this.correcto == false){
        Swal.fire('No se pudo consultar !! ',"La mision de trabajo consultada no existe ", 'error');
      }
    });
  }
}