import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { RegionalService } from 'src/app/services/regional.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Regional } from 'src/app/models/regional';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-regional-list',
  templateUrl: './regional-list.component.html',
  styleUrls: ['./regional-list.component.css']
})
export class RegionalListComponent implements OnInit {

 
  displayedColums:string[] = ['numeroRegional','nombreRegional','opciones'];
  regionales:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<Regional>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public regionalService:RegionalService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.regionalService.getregionalList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(regionales=>{
      this.regionales =regionales;
      this.dataSource=new MatTableDataSource(this.regionales);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(numeroRegional:string,nombreRegional:string){
    this.regionalService.getregionalList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(numeroRegional == value.numeroRegional){
          var key = document.id;
          this.regionalService.deleteregional(key);
          Swal.fire('Se Elimino con exito !! ', "Todo correcto ", 'success');
          this.nombre =nombreRegional ;
        }
      });
    });
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
