import { Component, OnInit } from '@angular/core';
import { RegionalService } from 'src/app/services/regional.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-regional-edit',
  templateUrl: './regional-edit.component.html',
  styleUrls: ['./regional-edit.component.css']
})
export class RegionalEditComponent implements OnInit {
  public regional:any;
  public numeroRegional:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public regionalService:RegionalService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadRegional();
  }

  loadRegional(){
    let params=this.activatedRoute.params['_value'];
    this.numeroRegional = params.regionalNumero;
    var regional = {
      nombreRegional:'',
      numeroRegional:0,
    }
    regional.numeroRegional = parseInt(params.regionalNumero);
    this.regionalService.getregionalList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroRegional == value.numeroRegional){
          regional.nombreRegional = value.nombreRegional;
          this.regional = regional;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.regionalService.getregionalList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroRegional == value.numeroRegional){
          var key = document.id;
          this.regionalService.updateregional(key,this.regional);
          Swal.fire('Se actualizo regional con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
