import { Component, OnInit } from '@angular/core';
import { Resultado } from 'src/app/models/resultado';
import { ResultadoService } from 'src/app/services/resultado.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-resultado',
  templateUrl: './guardar-resultado.component.html',
  styleUrls: ['./guardar-resultado.component.css']
})
export class GuardarResultadoComponent implements OnInit {


  resultado:Resultado = new Resultado();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private resultadoService:ResultadoService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.resultadoService.getResultadoList().get().subscribe(response=>{
      var contador = 0;
      var valor =1;
      while(contador<response.docs.length){
        response.forEach(document=>{
          if(valor ==document.data().numeroResultado){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.resultado.numeroResultado = valor;
      if(this.resultado.numeroResultado!=undefined && this.resultado.nombreResultado!=undefined){
        this.resultadoService.createResultado(this.resultado);
        this.resultado = new Resultado();
        Swal.fire('Se creo el resultado con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo el resultado !! ',this.tipoError, 'error');
      }
    });
  }

  onSubmit(){
    this.save();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
