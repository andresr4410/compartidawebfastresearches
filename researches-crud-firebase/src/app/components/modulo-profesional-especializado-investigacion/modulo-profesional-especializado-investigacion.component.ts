import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-modulo-profesional-especializado-investigacion',
  templateUrl: './modulo-profesional-especializado-investigacion.component.html',
  styleUrls: ['./modulo-profesional-especializado-investigacion.component.css']
})
export class ModuloProfesionalEspecializadoInvestigacionComponent implements OnInit {
  sideBarOpen = true;
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  constructor(public authService:AuthService,
    public router:Router) { }


  ngOnInit(): void {
  }



  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  toggleSideBar(){
    this.sideBarOpen = !this.sideBarOpen;
  }
  
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

}
