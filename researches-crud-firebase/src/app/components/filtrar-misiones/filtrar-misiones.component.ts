import { Component, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import { MatTableDataSource } from '@angular/material/table';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { PdfMakeWrapper, Table } from 'pdfmake-wrapper';
import pdfFonts from "pdfmake/build/vfs_fonts";

@Component({
  selector: 'app-filtrar-misiones',
  templateUrl: './filtrar-misiones.component.html',
  styleUrls: ['./filtrar-misiones.component.css']
})
export class FiltrarMisionesComponent implements OnInit {
  displayedColums:string[] = ['radicadoInterno','misionTrabajo','programa','ciudadano','defensorPublico','pag','fechaAsignacion','conductaPunible','investigador','diasVencimiento','estado','opciones',];
  public opcionEscogida:string ="";
  public escogioInvestigador:boolean=false;
  public escogioDefensorPublico:boolean=false;
  public desplegarTabla:boolean=false;
  public url:string="";
  public name:string="";
  public investigador:string="";
  public defensorPublico:string="";
  public fechaEscogida:string ="";
  public fechaEscogidaNumero:string = "";
  public investigadores:string[];
  public defensoresPublicos:string[];
  botonImprimir:boolean = false;
  filteredInvestigador: Observable<string[]>;
  myControlInvestigador:FormControl;
  myControlDefensorPublico:FormControl;
  filteredDefensorPublico:Observable<string[]>;

  dataSource:MatTableDataSource<MisionTrabajo>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public authService:AuthService,
    public router:Router,
    public misionTrabajoFilesService:MisionFileServicesService,
    public misionTrabajoService:MisionTrabajoService,
    public investigadorService:InvestigadorService,
    public defensorPublicoService:DefensorPublicoService) { }

  ngOnInit(): void {
    this.myControlInvestigador = new FormControl('');
    this.myControlDefensorPublico =new FormControl('');
    this.investigadores = ['',''];
    this.sacarInvestigadores(); 
    this.defensoresPublicos = ['',''];
    this.sacarDefensoresPublicos();
  }

  sacarInvestigadores(){
    this.investigadorService.investigadorRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.investigadores[contador] = value.nombre;
        contador+=1;
      });
      this.filteredInvestigador = this.myControlInvestigador.valueChanges
      .pipe(
        startWith(''),
        map(valueInvestigadores => this._filterInvestigadores(valueInvestigadores))
      );
    });
  }

  consultar(){
    this.desplegarTabla=false;
    var arregloMisionesNoNecesarias = new Array();
    var arregloMisionesInactivas= new Array();
    var arreglofechaNoNecesarias= new Array();
    if(this.fechaEscogida.localeCompare("enero")==0){
      this.fechaEscogidaNumero = "0";
    }else if(this.fechaEscogida.localeCompare("febrero")==0){
      this.fechaEscogidaNumero = "1";
    }else if(this.fechaEscogida.localeCompare("marzo")==0){
      this.fechaEscogidaNumero = "2";
    }else if(this.fechaEscogida.localeCompare("abril")==0){
      this.fechaEscogidaNumero = "3";
    }else if(this.fechaEscogida.localeCompare("mayo")==0){
      this.fechaEscogidaNumero = "4";
    }else if(this.fechaEscogida.localeCompare("junio")==0){
      this.fechaEscogidaNumero = "5";
    }else if(this.fechaEscogida.localeCompare("julio")==0){
      this.fechaEscogidaNumero = "6";
    }else if(this.fechaEscogida.localeCompare("agosto")==0){
      this.fechaEscogidaNumero = "7";
    }else if(this.fechaEscogida.localeCompare("septiembre")==0){
      this.fechaEscogidaNumero = "8";
    }else if(this.fechaEscogida.localeCompare("octubre")==0){
      this.fechaEscogidaNumero = "9";
    }else if(this.fechaEscogida.localeCompare("noviembre")==0){
      this.fechaEscogidaNumero = "10";
    }else if(this.fechaEscogida.localeCompare("diciembre")==0){
      this.fechaEscogidaNumero = "11";
    }
    if(this.fechaEscogida!=undefined && this.investigador.localeCompare("")!=0 && this.escogioInvestigador==true){
      this.desplegarTabla=true;
      this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
        map(changes=>
            changes.map(c=>
              ({key: c.payload.doc.id,  ...c.payload.doc.data()})
            )
          )
      ).subscribe(misionesTrabajos=>{
        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.investigador.localeCompare(this.investigador)!=0){
            arregloMisionesNoNecesarias.push(misionesTrabajos.indexOf(datosMision));
          }
        });
        var contador =0;
        for( var i = 0; i < arregloMisionesNoNecesarias.length; i++){
          misionesTrabajos.splice(arregloMisionesNoNecesarias[contador], 1);
          for (let index = 0; index < arregloMisionesNoNecesarias.length; index++) {
            arregloMisionesNoNecesarias[index] = arregloMisionesNoNecesarias[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.estado.localeCompare("Inactivo")==0){
            arregloMisionesInactivas.push(misionesTrabajos.indexOf(datosMision));
          }
        });

        var contador =0;
        for( var i = 0; i < arregloMisionesInactivas.length; i++){
          misionesTrabajos.splice(arregloMisionesInactivas[contador], 1);
          for (let index = 0; index < arregloMisionesInactivas.length; index++) {
            arregloMisionesInactivas[index] = arregloMisionesInactivas[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        misionesTrabajos.forEach(datosMision=>{
          let fecha = new Date(datosMision.fechaAsignacion);
          if(String(fecha.getMonth()).localeCompare(this.fechaEscogidaNumero)!=0){
            arreglofechaNoNecesarias.push(misionesTrabajos.indexOf(datosMision));
          }
        });

        var contador =0;
        for( var i = 0; i < arreglofechaNoNecesarias.length; i++){
          misionesTrabajos.splice(arreglofechaNoNecesarias[contador], 1);
          for (let index = 0; index < arreglofechaNoNecesarias.length; index++) {
            arreglofechaNoNecesarias[index] = arreglofechaNoNecesarias[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        if(misionesTrabajos.length==0){
          Swal.fire('No cuenta con misiones de trabajo activas!! ',"Esta persona no cuenta con misiones de trabajo activas", 'error');
        }else{
          this.botonImprimir = true;
        }
        this.dataSource=new MatTableDataSource(misionesTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      });
    }else if(this.fechaEscogida!=undefined && this.defensorPublico.localeCompare("")!=0 && this.escogioDefensorPublico==true){
      this.desplegarTabla=true;
      this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
        map(changes=>
            changes.map(c=>
              ({key: c.payload.doc.id,  ...c.payload.doc.data()})
            )
          )
      ).subscribe(misionesTrabajos=>{
        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.defensorPublico.localeCompare(this.defensorPublico)!=0){
            arregloMisionesNoNecesarias.push(misionesTrabajos.indexOf(datosMision));
          }
        });
        var contador =0;
        for( var i = 0; i < arregloMisionesNoNecesarias.length; i++){
          misionesTrabajos.splice(arregloMisionesNoNecesarias[contador], 1);
          for (let index = 0; index < arregloMisionesNoNecesarias.length; index++) {
            arregloMisionesNoNecesarias[index] = arregloMisionesNoNecesarias[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.estado.localeCompare("Inactivo")==0){
            arregloMisionesInactivas.push(misionesTrabajos.indexOf(datosMision));
          }
        });

        var contador =0;
        for( var i = 0; i < arregloMisionesInactivas.length; i++){
          misionesTrabajos.splice(arregloMisionesInactivas[contador], 1);
          for (let index = 0; index < arregloMisionesInactivas.length; index++) {
            arregloMisionesInactivas[index] = arregloMisionesInactivas[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        misionesTrabajos.forEach(datosMision=>{
          let fecha = new Date(datosMision.fechaAsignacion);
          if(String(fecha.getMonth()).localeCompare(this.fechaEscogidaNumero)!=0){
            arreglofechaNoNecesarias.push(misionesTrabajos.indexOf(datosMision));
          }
        });

        var contador =0;
        for( var i = 0; i < arreglofechaNoNecesarias.length; i++){
          misionesTrabajos.splice(arreglofechaNoNecesarias[contador], 1);
          for (let index = 0; index < arreglofechaNoNecesarias.length; index++) {
            arreglofechaNoNecesarias[index] = arreglofechaNoNecesarias[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        if(misionesTrabajos.length==0){
          Swal.fire('No cuenta con misiones de trabajo activas!! ',"Esta persona no cuenta con misiones de trabajo activas", 'error');
        }else{
          this.botonImprimir = true;
        }
        this.dataSource=new MatTableDataSource(misionesTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      });
    }else{
      Swal.fire('No escogio los datos requeridos!! ',"informacion no diligenciada ", 'error');
    }
  }

  sacarDefensoresPublicos(){
    this.defensorPublicoService.defensorPublicoRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.defensoresPublicos[contador] = value.nombre;
        contador+=1;
      });
      this.filteredDefensorPublico = this.myControlDefensorPublico.valueChanges
      .pipe(
        startWith(''),
        map(valueDefensorPublico => this._filterDefensorPublico(valueDefensorPublico))
      );
    });
  }

  private _filterInvestigadores(valueInvestigador: string): string[] {
    const filterValueInvestigador = valueInvestigador.toLowerCase();
    return this.investigadores.filter(optionInvestigador => optionInvestigador.toLowerCase().indexOf(filterValueInvestigador)===0);
  }

  private _filterDefensorPublico(valueDefensorPublico: string): string[] {
    const filterValueDefensorPublico = valueDefensorPublico.toLowerCase();
    return this.defensoresPublicos.filter(OptionDefensorPublico => OptionDefensorPublico.toLowerCase().indexOf(filterValueDefensorPublico)===0);
  }

  imprimir(){
    PdfMakeWrapper.setFonts(pdfFonts);
 
    const pdf = new PdfMakeWrapper();
 
    pdf.add('REPORTE MISIONES DE TRABAJO');
    
    pdf.defaultStyle({
      bold: true,
      fontSize: 11,
      alignment:"center"
    });
    pdf.pageSize({
      width: 990.28,
      height: 700
    });
    var tabla =new Table([
    [ this.displayedColums[0],this.displayedColums[1],this.displayedColums[2],this.displayedColums[3],this.displayedColums[4],this.displayedColums[5],this.displayedColums[6],this.displayedColums[7],
    this.displayedColums[8],this.displayedColums[9]]
    ]).end;
    for (let index = 0; index < this.dataSource.data.length; index++) {
      tabla.table.body.push([this.dataSource.data[index].radicadoInterno,this.dataSource.data[index].misionTrabajo,
        this.dataSource.data[index].programa,this.dataSource.data[index].ciudadano,this.dataSource.data[index].defensorPublico,
        this.dataSource.data[index].pag,this.dataSource.data[index].fechaAsignacion,this.dataSource.data[index].conductaPunible,
        this.dataSource.data[index].investigador,this.dataSource.data[index].diasVencimiento]);
    }
    pdf.add('MES : '+this.fechaEscogida.toLocaleUpperCase());
    pdf.add('Total : '+ this.dataSource.data.length);
    pdf.add(tabla);
    pdf.create().open();
  }

  seleccionarOpcion(){
    this.escogioDefensorPublico = false;
    this.escogioInvestigador =false;
    if(this.opcionEscogida.localeCompare("")==0){
      Swal.fire('No selecciono ninguna opcion correcta !! ',"Ninguna opcion de filtrado", 'error');
    }else if(this.opcionEscogida.localeCompare("investigador")==0){
      Swal.fire('Escoja investigador y mes!! ',"Se mostraran las misiones de trabajo de las opciones seleccionadas a continuacion : ", 'success');
      this.escogioInvestigador=true;
    }else{
      this.escogioDefensorPublico=true;
      Swal.fire('Escoja defensor publico y mes!! ',"Se mostraran las misiones de trabajo de las opciones seleccionadas a continuacion : ", 'success');
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }


  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }



  descargarEvidencia(radicadoInterno:string){
    var radicadoString1 = String(radicadoInterno+".docx");
        var radicadoString2 = String(radicadoInterno+".pptx");
        var radicadoString3 = String(radicadoInterno+".pdf");
        var radicadoString4 = String(radicadoInterno+".rar");
        var radicadoString5 = String(radicadoInterno+".zip");
        this.misionTrabajoFilesService.getUploadList().get().subscribe(response=>{
          response.docs.forEach(data=>{
            const value =data.data();
            if(radicadoString1.localeCompare(value.name)==0 || radicadoString2.localeCompare(value.name)==0
            || radicadoString3.localeCompare(value.name)==0 || radicadoString4.localeCompare(value.name)==0
            || radicadoString5.localeCompare(value.name)==0){
              this.url = value.url;
              this.name = value.name;
            }
          });
        });
  }

  
  getColor(inUtil) {
    if (inUtil <0)
        return 'red';
    else
        return 'green';
  }
  
}
