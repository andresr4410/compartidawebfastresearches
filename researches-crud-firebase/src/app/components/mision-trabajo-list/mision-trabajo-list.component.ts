import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-mision-trabajo-list',
  templateUrl: './mision-trabajo-list.component.html',
  styleUrls: ['./mision-trabajo-list.component.css']
})
export class MisionTrabajoListComponent implements OnInit {
  displayedColums:string[] = ['radicadoInterno','misionTrabajo','programa','ciudadano','defensorPublico','pag','regional','estadoProceso','resultado','investigacion','investigador','diasVencimiento','estado','opciones',];
  misionTrabajos:any;
  activarDescarga = false;
  descargarEvidenciaCorrecto:boolean=false;
  public correcto:boolean=false;
  public radicadoInterno:string="";
  url:string="";
  name:string="";
  estadoActualMision:string="";
  diasVencimiento:number = 0;
  puedeCambiarEstado:boolean = false;

  dataSource:MatTableDataSource<MisionTrabajo>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public misionTrabajoService:MisionTrabajoService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router,
    public misionTrabajoFilesService:MisionFileServicesService) { }

  ngOnInit(): void {
    var resultado = JSON.parse(localStorage.getItem(('usuario')));
      if(resultado.idTipoUsuario == 2 ){
        this.puedeCambiarEstado = true;
      }else{
        this.puedeCambiarEstado = false;
      }
    this.inicializarLista();
  }

  inicializarLista(){
    this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(misionTrabajos=>{
      var arregloValores = new Array();
      misionTrabajos.forEach(data=>{
        var fechaActual = new Date();
        let fecha1 = new Date(fechaActual);
        let fecha2 = new Date(data.fechaVencimiento);
        let resta = fecha2.getTime() - fecha1.getTime();
        var resultado = Math.round(resta/ (1000*60*60*24));
        data.diasVencimiento = resultado;
      }); 
      var resultado = JSON.parse(localStorage.getItem(('usuario')));
      if(resultado.idTipoUsuario == 2 ){
        this.dataSource=new MatTableDataSource(misionTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      }else{
        misionTrabajos.forEach(datosMision=>{
          if(datosMision.estado.localeCompare("Activo")!=0){
            arregloValores.push(misionTrabajos.indexOf(datosMision));
          }
        });
        var contador =0;
        for( var i = 0; i < arregloValores.length; i++){
          misionTrabajos.splice(arregloValores[contador], 1);
          for (let index = 0; index < arregloValores.length; index++) {
            arregloValores[index] = arregloValores[index] - 1 ;
          }
          contador  = contador + 1 ;
        }
        this.dataSource=new MatTableDataSource(misionTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      }
    });
  }

  getColor(inUtil) {
    if (inUtil <0)
        return 'red';
    else
        return 'green';
  }
  cambioEstado(misionTrabajo:string){
    this.misionTrabajoService.getMisionTrabajoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(misionTrabajo == value.misionTrabajo){
          var key = document.id;
          if(value.estadoActivo==true){
            value.estado = "Inactivo";
            value.estadoActivo = false;
          }else{
            value.estado ="Activo";
            value.estadoActivo = true;
          }
          this.misionTrabajoService.updateMisionTrabajo(key,value);
          Swal.fire('Se Actualizo correctamente !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

  descargarEvidencia(radicadoInterno:string){
        this.activarDescarga = false;
        var radicadoString1 = String(radicadoInterno+".docx");
        var radicadoString2 = String(radicadoInterno+".pptx");
        var radicadoString3 = String(radicadoInterno+".pdf");
        var radicadoString4 = String(radicadoInterno+".rar");
        var radicadoString5 = String(radicadoInterno+".zip");
        this.misionTrabajoFilesService.getUploadList().get().subscribe(response=>{
          response.docs.forEach(data=>{
            const value =data.data();
            if(radicadoString1.localeCompare(value.name)==0 || radicadoString2.localeCompare(value.name)==0
            || radicadoString3.localeCompare(value.name)==0 || radicadoString4.localeCompare(value.name)==0
            || radicadoString5.localeCompare(value.name)==0){
              this.url = value.url;
              this.name = value.name;
              this.activarDescarga = true;
            }
          });
          if(this.activarDescarga==false){
            Swal.fire('No existe  !! ', "Este radicado interno no tiene evidencia en el momento ", 'error');
          }
        });
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

}
