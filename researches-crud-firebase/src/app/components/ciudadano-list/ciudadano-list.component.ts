import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Ciudadano } from 'src/app/models/ciudadano';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { CiudadanoService } from 'src/app/services/ciudadano.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ciudadano-list',
  templateUrl: './ciudadano-list.component.html',
  styleUrls: ['./ciudadano-list.component.css']
})
export class CiudadanoListComponent implements OnInit {

  displayedColums:string[] = ['nombre','apellido','correo','cedula','direccion','edad','ciudad','telefono','opciones'];
  ciudadanos:any;
  public correcto:boolean=false;
  public cedula:string="";
  public validarBorrado:boolean = false;

  dataSource:MatTableDataSource<Ciudadano>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public ciudadanoService:CiudadanoService,
    public matSnackBar:MatSnackBar,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    this.validarBorrado =false;
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.validarBorrado = true;
    }else{
      this.validarBorrado = false;
    }
    this.inicializarLista();
  }

  inicializarLista(){
    this.ciudadanoService.getCiudadanoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(ciudadanos=>{
      this.ciudadanos = ciudadanos;
      this.dataSource=new MatTableDataSource(this.ciudadanos);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(cedula:string){
    this.ciudadanoService.getCiudadanoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(cedula == value.cedula){
          var key = document.id;
          this.ciudadanoService.deleteCiudadano(key);
          Swal.fire('Se elimino correctamente !! ', "Todo correcto ", 'success');
          this.cedula = cedula;
        }
      });
    });
  }

  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
