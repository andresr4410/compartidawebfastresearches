import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { AuthService } from 'src/app/services/auth.service';
import { TipoUsuario } from 'src/app/models/tipoUsuario';
import { TipoUsuarioService } from 'src/app/services/tipo-usuario.service';
import { FormBuilder} from "@angular/forms";
import { Router } from '@angular/router';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-guardar-usuario',
  templateUrl: './guardar-usuario.component.html',
  styleUrls: ['./guardar-usuario.component.css']
})
export class GuardarUsuarioComponent implements OnInit {
  public usuario:Usuario = new Usuario("",0,0,"","",true);
  public submitted:boolean = false;
  public incorrecto:boolean=false;
  public correcto:boolean=false;
  public tipoError:string = "";
  public listaUsuarios:Usuario[];
  public cantidadUsuario:number;
  public tipoUsuarioList:TipoUsuario[];
  public tipoUsuario:TipoUsuario;
  public opciones:string[];
  opcion:string;

  constructor(private usuarioService:UsuarioService,
    private authenticationService:AuthService,
    private tipousuarioService:TipoUsuarioService,
    public fb: FormBuilder,
    public router:Router,
    public authService:AuthService) { }
  

  ngOnInit(): void {
    this.opciones = ['',''];
    this.sacarOpciones();
  }

  onSubmit(){
    this.save();
  }

  save(){
    this.usuarioService.getUsuarioList().get().subscribe(data =>{
      if(this.opcion==undefined){
        this.opcion ="";
        this.usuario.idTipoUsuario = -1;
      }
      if(this.opcion.localeCompare("Ingeniero")==0){
        this.usuario.idTipoUsuario = 2;
      }
      if(this.opcion.localeCompare("Auxiliar-administrativo")==0){
        this.usuario.idTipoUsuario = 1;
      }
      if(this.opcion.localeCompare("Coordinador-PAG")==0){
        this.usuario.idTipoUsuario = 3;
      }
      if(this.opcion.localeCompare("Tecnico-criminalista")==0){
        this.usuario.idTipoUsuario = 4;
      }
      if(this.opcion.localeCompare("Profesional-especializado-investigacion")==0){
        this.usuario.idTipoUsuario = 5;
      }
      if(this.opcion.localeCompare("Profesional-especialista-criminalistica")==0){
        this.usuario.idTipoUsuario = 6;
      }
      if(this.opcion.localeCompare("Defensor-publico")==0){
        this.usuario.idTipoUsuario = 7;
      }
      var contador = 0;
      var valor =1;
      while(contador<data.docs.length){
        data.forEach(document=>{
          if(valor ==document.data().idUsuario){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.usuario.idUsuario = valor;
      this.authenticationService.signup(this.usuario);
    });
  }
  sacarOpciones(){
    this.tipousuarioService.tipoUsuarioRef.get().subscribe(response=>{
      var contador = 0;
      response.forEach(document=>{
        const value = document.data();
        this.opciones[contador] = value.rol;
        contador+=1;
      });
    });
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['listaUsuario']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['listaUsuario']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['listaUsuario']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['listaUsuario']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['listaUsuario']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['listaUsuario']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
}
