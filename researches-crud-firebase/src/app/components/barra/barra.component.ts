import { Component, OnInit, ChangeDetectorRef, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { map } from 'rxjs/operators';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
@Component({
  selector: 'app-barra',
  templateUrl: './barra.component.html',
  styleUrls: ['./barra.component.css']
})
export class BarraComponent implements OnInit{
  misionesActivas:number=0;
  misionesInactivas:number=0;
  misionesTotales:number=0;
  
  public barChartLabels: Label[] = ['','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''
  ,'','','','',''];
  public pieChartPlugins = [pluginDataLabels];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public investigadores:string[];
  public temporalInvestigadores:string[]=['','','','','','','','','','','',''];
  public barChartData: ChartDataSets[];
  public valoresActivos:number[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  public valoresInactivos:number[]=[0,0,0,0,0,0,0,0,0,0,0,0,0,0];

  constructor(public authService:AuthService,
    public router:Router,
    public investigadorService:InvestigadorService,
    public misionTrabajoService:MisionTrabajoService) { }

  ngOnInit(): void {
    this.barChartLabels.length = 1;
    this.barChartData =  [
      { data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: 'Activo' },
      { data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0], label: 'Inactivo' }
    ];
    this.llenarData();
  }
  
  llenarData(){
    this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(misionesTrabajos=>{
      this.misionesTotales = misionesTrabajos.length;
      misionesTrabajos.forEach(data=>{
        if(data.estado.localeCompare("Activo")==0){
          this.misionesActivas+=1;
        }else{
          this.misionesInactivas+=1;
        }
      });
      this.investigadorService.investigadorRef.get().subscribe(response=>{
        var contador = 0;
        this.barChartLabels.length = response.size;
        this.temporalInvestigadores.length = response.size;
        this.valoresActivos.length = response.size;
        this.valoresInactivos.length = response.size;
        response.forEach(data=>{
          const value = data.data();
          this.barChartLabels[contador] = value.nombre;
          this.temporalInvestigadores[contador]=value.nombre;
          contador+=1;
        });
        misionesTrabajos.forEach(response=>{
          console.log(response.investigador);
          for (let index = 0; index < this.temporalInvestigadores.length; index++) {
            console.log(this.temporalInvestigadores[index]);
            console.log(response.investigador);
            if(this.temporalInvestigadores[index].localeCompare(response.investigador)==0 && this.barChartData[0].label.localeCompare(response.estado)==0){
              this.valoresActivos[index] +=1;
            }else if(this.temporalInvestigadores[index].localeCompare(response.investigador)==0 && this.barChartData[1].label.localeCompare(response.estado)==0){
              this.valoresInactivos[index]+=1;
            }
          }      
        })
        for (let index = 0; index < this.valoresActivos.length; index++) {
          console.log(this.valoresActivos[index]);
          this.barChartData[0].data[index]=this.valoresActivos[index];
        }
        for (let index = 0; index < this.valoresInactivos.length; index++) {
          this.barChartData[1].data[index] = this.valoresInactivos[index];
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
}

