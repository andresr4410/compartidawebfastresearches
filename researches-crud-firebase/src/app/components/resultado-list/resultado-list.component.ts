import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ConductaPunible } from 'src/app/models/conductaPunible';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ResultadoService } from 'src/app/services/resultado.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resultado-list',
  templateUrl: './resultado-list.component.html',
  styleUrls: ['./resultado-list.component.css']
})
export class ResultadoListComponent implements OnInit {

  displayedColums:string[] = ['numeroResultado','nombreResultado','opciones'];
  resultados:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<ConductaPunible>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public resultadoService:ResultadoService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router ) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.resultadoService.getResultadoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(resultados=>{
      this.resultados =resultados;
      this.dataSource=new MatTableDataSource(this.resultados);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(numeroResultado:string,nombreResultado:string){
    this.resultadoService.getResultadoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(numeroResultado == value.numeroResultado){
          var key = document.id;
          this.resultadoService.deleteResultado(key);
          Swal.fire('Se Elimino con exito !! ', "Todo correcto ", 'success');
          this.nombre =nombreResultado ;
        }
      });
    });
  }
  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
