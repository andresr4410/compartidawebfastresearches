import { Component, OnInit, ViewChild } from '@angular/core';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { MatPaginator } from '@angular/material/paginator';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { UsuarioService } from 'src/app/services/usuario.service';
import { InvestigadorService } from 'src/app/services/investigador.service';
import { ContentObserver } from '@angular/cdk/observers';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-misiones-correo',
  templateUrl: './misiones-correo.component.html',
  styleUrls: ['./misiones-correo.component.css']
})
export class MisionesCorreoComponent implements OnInit {
  displayedColums:string[] = ['radicadoInterno','misionTrabajo','programa','ciudadano','defensorPublico','pag','regional','estadoProceso','resultado','investigacion','investigador','diasVencimiento','estado','opciones'];
  misionTrabajos:any;
  misionTrabajosTemp:MisionTrabajo[];
  public correcto:boolean=false;
  public radicadoInterno:string="";
  nombreInvestigador:string="";
  public mision:MisionTrabajo = new MisionTrabajo;
  public activarDescarga:boolean =false;
  public name:string="";
  public url:string="";


  dataSource:MatTableDataSource<MisionTrabajo>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public misionTrabajoService:MisionTrabajoService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router,
    public usuarioService:UsuarioService,
    public investigadorService:InvestigadorService,
    public misionTrabajoFilesService:MisionFileServicesService) { }

  ngOnInit(): void {
    this.mision.radicadoInterno ="";
    this.mision.misionTrabajo ="";
    this.mision.programa ="";
    this.mision.ciudadano="";
    this.mision.programa ="";
    this.mision.defensorPublico ="";
    this.mision.pag ="";
    this.mision.estadoProceso="";
    this.mision.resultado ="";
    this.mision.investigacion ="";
    this.mision.investigador ="";
    this.inicializarLista();
  }

  inicializarLista(){
    var usuario = {
      email:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    this.investigadorService.investigadorRef.get().subscribe(response=>{
      response.forEach(data=>{
        if(usuario.email.localeCompare(data.data().email)==0){
          this.nombreInvestigador = data.data().nombre;
        }
      });
      var mision = {
        radicadoInterno:'',
        misionTrabajo:'',
        programa:'',
        ciudadano:'',
        defensorPublico:'',
        pag:'',
        regional:'',
        estadoProceso:'',
        resultado:'',
        investigacion:'',
        investigador:'',
      }
      var arregloValores = new Array();
      this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
        map(changes=>
            changes.map(c=>
              ({key: c.payload.doc.id,  ...c.payload.doc.data()})
            )
          )
      ).subscribe(misionesTrabajos=>{
        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.investigador.localeCompare(this.nombreInvestigador)!=0){
            arregloValores.push(misionesTrabajos.indexOf(datosMision));
          }
        });
        var contador =0;
        for( var i = 0; i < arregloValores.length; i++){
          misionesTrabajos.splice(arregloValores[contador], 1);
          for (let index = 0; index < arregloValores.length; index++) {
            arregloValores[index] = arregloValores[index] - 1 ;
          }
          contador  = contador + 1 ;
        }
        var arregloMisionesInactivas = new Array();
        
        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.estado.localeCompare("Inactivo")==0){
            arregloMisionesInactivas.push(misionesTrabajos.indexOf(datosMision));
          }
        });

        var contador =0;
        for( var i = 0; i < arregloMisionesInactivas.length; i++){
          misionesTrabajos.splice(arregloMisionesInactivas[contador], 1);
          for (let index = 0; index < arregloMisionesInactivas.length; index++) {
            arregloMisionesInactivas[index] = arregloMisionesInactivas[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        this.dataSource=new MatTableDataSource(misionesTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      });
    });
  }

  descargarEvidencia(radicadoInterno:string){
    this.activarDescarga = false;
    var radicadoString1 = String(radicadoInterno+".docx");
    var radicadoString2 = String(radicadoInterno+".pptx");
    var radicadoString3 = String(radicadoInterno+".pdf");
    var radicadoString4 = String(radicadoInterno+".rar");
    var radicadoString5 = String(radicadoInterno+".zip");
    this.misionTrabajoFilesService.getUploadList().get().subscribe(response=>{
      response.docs.forEach(data=>{
        const value =data.data();
        if(radicadoString1.localeCompare(value.name)==0 || radicadoString2.localeCompare(value.name)==0
        || radicadoString3.localeCompare(value.name)==0 || radicadoString4.localeCompare(value.name)==0
        || radicadoString5.localeCompare(value.name)==0){
          this.url = value.url;
          this.name = value.name;
          this.activarDescarga = true;
        }
      });
      if(this.activarDescarga==false){
        Swal.fire('No existe  !! ', "Este radicado interno no tiene evidencia en el momento ", 'error');
      }
    });
  }
  getColor(inUtil) {
    if (inUtil <0)
        return 'red';
    else
        return 'green';
  }
    
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

}
