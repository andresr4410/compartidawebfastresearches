import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { EstadoProceso } from 'src/app/models/estadoProceso';
import { MatPaginator } from '@angular/material/paginator';
import { EstadoProcesoService } from 'src/app/services/estado-proceso.service';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-estado-proceso-list',
  templateUrl: './estado-proceso-list.component.html',
  styleUrls: ['./estado-proceso-list.component.css']
})
export class EstadoProcesoListComponent implements OnInit {

  displayedColums:string[] = ['numeroProceso','nombreProceso','opciones'];
  estadoProcesos:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<EstadoProceso>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public estadoProcesoService:EstadoProcesoService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.estadoProcesoService.getEstadoProcesoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(estadoProcesos=>{
      this.estadoProcesos =estadoProcesos;
      this.dataSource=new MatTableDataSource(this.estadoProcesos);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(numeroProceso:string,nombreProceso:string){
    this.estadoProcesoService.getEstadoProcesoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(numeroProceso == value.numeroProceso){
          var key = document.id;
          this.estadoProcesoService.deleteEstadoProceso(key);
          Swal.fire('Se elimino correctamente !! ', "Todo correcto ", 'success');
          this.nombre =nombreProceso ;
        }
      });
    });
  }

  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag'])
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
}
