import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { MisionTrabajo } from 'src/app/models/misionTrabajo';
import { AuthService } from 'src/app/services/auth.service';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';

@Component({
  selector: 'app-consultar-informe-final',
  templateUrl: './consultar-informe-final.component.html',
  styleUrls: ['./consultar-informe-final.component.css']
})
export class ConsultarInformeFinalComponent implements OnInit {

  displayedColums:string[] = ['misionTrabajo','ciudadano','defensorPublico','pag','estadoProceso','investigacion','investigador','conductaPunible','evidencia'];
  misionTrabajos:any;
  misionTrabajosTemp:MisionTrabajo[];
  public correcto:boolean=false;
  public radicadoInterno:string="";
  nombreDefensorPublico:string="";
  public mision:MisionTrabajo = new MisionTrabajo;
  public activarDescarga:boolean =false;
  public name:string="";
  public url:string="";
  constructor(public router:Router,
    public authService:AuthService,
    public defensorPublicoService:DefensorPublicoService,
    public misionTrabajoService:MisionTrabajoService,
    public misionTrabajoFilesService:MisionFileServicesService) { }
  dataSource:MatTableDataSource<MisionTrabajo>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
 
 
 
  ngOnInit(): void {
    this.mision.radicadoInterno ="";
    this.mision.misionTrabajo ="";
    this.mision.programa ="";
    this.mision.ciudadano="";
    this.mision.programa ="";
    this.mision.defensorPublico ="";
    this.mision.pag ="";
    this.mision.estadoProceso="";
    this.mision.resultado ="";
    this.mision.investigacion ="";
    this.mision.investigador ="";
    this.mision.conductaPunible ="";
    this.inicializarLista();
  }
  inicializarLista(){
    var usuario = {
      email:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    this.defensorPublicoService.defensorPublicoRef.get().subscribe(response=>{
      response.forEach(data=>{
        if(usuario.email.localeCompare(data.data().email)==0){
          this.nombreDefensorPublico = data.data().nombre;
        }
      });
      var arregloValores = new Array();
      this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
        map(changes=>
            changes.map(c=>
              ({key: c.payload.doc.id,  ...c.payload.doc.data()})
            )
          )
      ).subscribe(misionesTrabajos=>{
        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.defensorPublico.localeCompare(this.nombreDefensorPublico)!=0){
            arregloValores.push(misionesTrabajos.indexOf(datosMision));
          }
        });
        var contador =0;
        for( var i = 0; i < arregloValores.length; i++){
          misionesTrabajos.splice(arregloValores[contador], 1);
          for (let index = 0; index < arregloValores.length; index++) {
            arregloValores[index] = arregloValores[index] - 1 ;
          }
          contador  = contador + 1 ;
        }

        var arregloMisionesInactivas = new Array();
        
        misionesTrabajos.forEach(datosMision=>{
          if(datosMision.estado.localeCompare("Inactivo")==0){
            arregloMisionesInactivas.push(misionesTrabajos.indexOf(datosMision));
          }
        });

        var contador =0;
        for( var i = 0; i < arregloMisionesInactivas.length; i++){
          misionesTrabajos.splice(arregloMisionesInactivas[contador], 1);
          for (let index = 0; index < arregloMisionesInactivas.length; index++) {
            arregloMisionesInactivas[index] = arregloMisionesInactivas[index] - 1 ;
          }
          contador  = contador + 1 ;
        }
        this.dataSource=new MatTableDataSource(misionesTrabajos);
        this.dataSource.paginator=this.paginator;
        this.dataSource.sort=this.sort;
      });
    });
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

    
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
