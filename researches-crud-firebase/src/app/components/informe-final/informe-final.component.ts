import { Component,IterableDiffers, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FileServicesService } from 'src/app/services/file-services.service';
import { AngularFirestoreCollection} from '@angular/fire/firestore';
import { AngularFirestore} from '@angular/fire/firestore';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { InformeFinalService } from 'src/app/services/informe-final.service';
import { InformeFinal } from 'src/app/models/informe-final';
import { ItemInforme } from 'src/app/models/item-informe';
import { ItemInformeService } from 'src/app/services/item-informe.service';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-informe-final',
  templateUrl: './informe-final.component.html',
  styleUrls: ['./informe-final.component.css']
})
export class InformeFinalComponent implements OnInit,OnDestroy{

  //Contenedores de las evidencias subidas por el investigador criminal
  itemsCollectionImages:AngularFirestoreCollection<ItemInforme>;
  itemsCollectionArchives:AngularFirestoreCollection<ItemInforme>;

  //Contendra todas las evidencias que contiene la mision de trabajo seleccionada previamente
  itemsImages:ItemInforme[] =[];
  itemsArchives:ItemInforme[] =[];
  itemsFinales:ItemInforme[] = [];
  itemsFinalesImages:Observable<ItemInforme[]>;
  itemsFinalesArchives:Observable<ItemInforme[]>;
  //Contendra todas las evidencias seleccionadas por el Aux, ingeniero o PAG
  informeFinalFinal:InformeFinal[] = [];


  evidencia = new ItemInforme();
  informeFinal = new InformeFinal();

  paramIdMision:any;
  idRegistro:number;

  constructor(public authService:AuthService, 
    public router:Router,
    public fileStorage:FileServicesService,
    public afs:AngularFirestore,
    public activatedRoute:ActivatedRoute,
    public misionDeTrabajoService:MisionTrabajoService, 
    public informeFinalService:InformeFinalService,
    public itemsFinalesService:ItemInformeService,
    public iterableDiffers: IterableDiffers) {
      this.idRegistro = 0;

      this.router.events
      .pipe(filter((rs): rs is NavigationEnd => rs instanceof NavigationEnd))
      .subscribe(event => {
        if (
          event.id === 1 &&
          event.url === event.urlAfterRedirects 
        ) {
          this.itemsFinalesService.getItemInformeList().get().subscribe(response=>{
            response.docs.forEach(document=>{
                var key = document.id;
                this.itemsFinalesService.deleteItemInforme(key);
            });
          });
        }
      })
  };

  ngOnInit(){
    this.filtrarEvidenciasMision();
  }

  filtrarEvidenciasMision() {
      this.paramIdMision=this.activatedRoute.params['_value'];
      //Collecciones que contendran la informacion de las evidencias realizadas por el investigador
      this.itemsCollectionImages = this.afs.collection<ItemInforme>('imagenesEvidenciaInvestigador');
      this.itemsCollectionArchives = this.afs.collection<ItemInforme>('documentosEvidenciaInvestigador');
      //Array que contendra las misiones de trabajo que no tienen nada que ver con la mision para borrarlas
      var arregloValoresImages = new Array();
      var arregloValoresArchives = new Array();
      this.itemsFinalesArchives = this.itemsCollectionArchives.valueChanges();
      this.itemsFinalesImages = this.itemsCollectionImages.valueChanges();

  }


  validarMision(mision:string){
    if(this.paramIdMision.idMision == mision){
      return true;
    }else{
      return false;
    }
  }


  eliminarEvidenciaSeleccionada(informe:any){
   this.itemsFinalesService.getItemInformeList().get().subscribe(response=>{
    response.docs.forEach(document=>{
      const value = document.data();
        if(informe.evidencia == value.evidencia){
          var key = document.id;
          this.itemsFinalesService.deleteItemInforme(key);
          const index = this.itemsFinales.indexOf(informe, 0);
          if (index > -1) {
            this.itemsFinales.splice(index, 1);
          }
        }
      });
    });
  }



  agregarInforme(item:any,tipoEvidencia:number){
    this.evidencia = new ItemInforme();
    this.idRegistro  = this.idRegistro + 1;
    if(tipoEvidencia == 0){
      this.evidencia.evidencia = item.image;
    }else{
      this.evidencia.evidencia = item.archive;
    }
    this.evidencia.idMision = item.idMision;
    this.evidencia.description = item.description;
    this.evidencia.identificacionImagen = tipoEvidencia.toString();
    this.evidencia.idRegistro =this.idRegistro.toString();
    this.evidencia.fecha = item.date;
    this.misionDeTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(misionTrabajo=>{
      misionTrabajo.forEach(mision=>{
        if(mision.misionTrabajo == this.evidencia.idMision){
          this.evidencia.defensorPublico = mision.defensorPublico;
          this.evidencia.investigador = mision.investigador;
        }
      });
      this.itemsFinalesService.createItemInforme(this.evidencia);
      this.itemsFinales.push(this.evidencia);
    });
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['/seleccionarInformeFinal']);
    }
  }

  enviarReporte(){
    this.itemsFinalesService.getItemInformeList().get().subscribe(response=>{
      response.forEach(data=>{
        this.informeFinal = new InformeFinal();
        var element =data.data();
        this.informeFinal.idMision = element.idMision;
        this.informeFinal.evidencia= element.evidencia;
        this.informeFinal.identificacionImagen= element.identificacionImagen;
        this.informeFinal.defensorPublico= element.defensorPublico;
        this.informeFinal.investigador= element.investigador;
        this.informeFinal.description= element.description;
        this.informeFinal.fecha= element.fecha;
        this.informeFinalService.createInformeFinal(this.informeFinal);
      });
      Swal.fire("Informe final creado con exito","Hecho!","success");
    });

    this.itemsFinalesService.getItemInformeList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        var key = document.id;
        this.itemsFinalesService.deleteItemInforme(key);
      });
    });
  }

  
  ngOnDestroy(){
    this.itemsFinalesService.getItemInformeList().get().subscribe(response=>{
      response.docs.forEach(document=>{
          var key = document.id;
          this.itemsFinalesService.deleteItemInforme(key);
      });
    });
   }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }


}
