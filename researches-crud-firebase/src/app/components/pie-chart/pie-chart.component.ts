import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MisionTrabajoService } from 'src/app/services/mision-trabajo.service';
import { DefensorPublicoService } from 'src/app/services/defensor-publico.service';
import { map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.css']
})
export class PieChartComponent implements OnInit {
  nombreDefensorPublico:string="";
  violenciaContraServidorPublicoCantidad:number=0;
  accesoCarnalViolento:number=0;
  hurtoCalificadoAgravado:number=0;
  hurtoCalificado:number=0;
  porteIlegal:number=0;
  hurtoSimple:number=0;
  porteEstuperfaciente:number = 0;
  accesoCarnalAbusivo:number=0;
  misionesActivas:number=0;
  misionesInactivas:number=0;
  misionesTotales:number=0;

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.datasets[value];
          return label;
        },
      },
    }
  };
  public pieChartPlugins = [pluginDataLabels];
  public pieChartLabels: Label[] = [];
  public pieChartData: number[];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['rgba(166,202,224,0.8)', 'rgba(0,255,0,0.5)', 'rgba(0,0,255,0.5)','rgba(6,21,60,0.5)',
    'rgba(89,53,31,1)','rgba(241,191,0,0.7)','rgba(255,40,0,0.8)','rgba(39,81,47,0.9)','rgba(0,0,0,0.9)'],
    },
  ];

  constructor(public router:Router,
    public authService:AuthService,
    public misionTrabajoService:MisionTrabajoService,
    public defensorPublicoService:DefensorPublicoService) { }

  ngOnInit() {
    this.pieChartData = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
    this.sacarDatosDefensor();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  sacarDatosDefensor(){
      var usuario = {
        email:'',
        idTipoUsuario:''
      }
      usuario =JSON.parse(localStorage.getItem('usuario'));
      this.defensorPublicoService.defensorPublicoRef.get().subscribe(response=>{
        response.forEach(data=>{
          if(usuario.email.localeCompare(data.data().email)==0){
            this.nombreDefensorPublico = data.data().nombre;
          }
        });
        var arregloValores = new Array();
        this.misionTrabajoService.getMisionTrabajoList().snapshotChanges().pipe(
          map(changes=>
              changes.map(c=>
                ({key: c.payload.doc.id,  ...c.payload.doc.data()})
              )
            )
        ).subscribe(misionesTrabajos=>{
          misionesTrabajos.forEach(datosMision=>{
            if(datosMision.defensorPublico.localeCompare(this.nombreDefensorPublico)!=0){
              arregloValores.push(misionesTrabajos.indexOf(datosMision));
            }
          });
          var contador =0;
          for( var i = 0; i < arregloValores.length; i++){
            misionesTrabajos.splice(arregloValores[contador], 1);
            for (let index = 0; index < arregloValores.length; index++) {
              arregloValores[index] = arregloValores[index] - 1 ;
            }
            contador  = contador + 1 ;
          }

          var arregloMisionesInactivas = new Array();
        
          misionesTrabajos.forEach(datosMision=>{
            if(datosMision.estado.localeCompare("Inactivo")==0){
              arregloMisionesInactivas.push(misionesTrabajos.indexOf(datosMision));
            }
          });
  
          var contador =0;
          for( var i = 0; i < arregloMisionesInactivas.length; i++){
            misionesTrabajos.splice(arregloMisionesInactivas[contador], 1);
            for (let index = 0; index < arregloMisionesInactivas.length; index++) {
              arregloMisionesInactivas[index] = arregloMisionesInactivas[index] - 1 ;
            }
            contador  = contador + 1 ;
          }
        
          this.misionesTotales = misionesTrabajos.length;
          if(misionesTrabajos.length!=0){
            var contadorPos=0;
            var bandera = false;
            misionesTrabajos.forEach(data=>{
              if(this.pieChartLabels.length==0){
                this.pieChartLabels[contadorPos]=data.conductaPunible;
                contadorPos+=1;
              }else{
                bandera =false;
                for (let index = 0; index < this.pieChartLabels.length; index++) {
                  if(String(this.pieChartLabels[index]).localeCompare(data.conductaPunible)==0){
                    bandera=true;
                  }
                }
                if(bandera==false){
                  this.pieChartLabels[contadorPos]=data.conductaPunible;
                  contadorPos+=1;
                }
                bandera = false;
              }
            });
          }
          misionesTrabajos.forEach(data=>{
            for (let index = 0; index < this.pieChartLabels.length; index++) {
              if(String(this.pieChartLabels[index]).localeCompare(data.conductaPunible)==0){
                this.pieChartData[index]+=1;
              }
            }
          });

          var maximo = this.pieChartData.length;
          while(maximo>this.pieChartLabels.length){
            this.pieChartData.pop();
            maximo-=1;
          }
          console.log(this.pieChartData.length);

        });
      });  
  }
}
