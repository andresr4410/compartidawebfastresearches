import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CiudadanoService } from 'src/app/services/ciudadano.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-ciudadano-edit',
  templateUrl: './ciudadano-edit.component.html',
  styleUrls: ['./ciudadano-edit.component.css']
})
export class CiudadanoEditComponent implements OnInit {
  public ciudadano:any;
  public cedula:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  public validarBorrado:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public ciudadanoService:CiudadanoService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
   
    this.loadCiudadano();
    
    
  }

  loadCiudadano(){
    let params=this.activatedRoute.params['_value'];
    this.cedula = params.cedulaCiudadano;
    var ciudadano = {
      nombre:'',
      apellido:'',
      edad:0,
      ciudad:'',
      telefono:0,
      direccion:'',
      email:'',
      cedula:''
    }
    ciudadano.cedula = params.cedulaCiudadano;
    this.ciudadanoService.getCiudadanoList().get().subscribe(response=>{
      console.log(this.cedula);
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          ciudadano.nombre = value.nombre;
          ciudadano.apellido = value.apellido;
          ciudadano.edad = value.edad;
          ciudadano.ciudad = value.ciudad;
          ciudadano.telefono = value.telefono;
          ciudadano.direccion = value.direccion;
          ciudadano.email = value.email;
          this.ciudadano = ciudadano;
        }
        if(this.cedula==undefined){
          ciudadano.nombre = value.nombre;
          ciudadano.apellido = value.apellido;
          ciudadano.edad = value.edad;
          ciudadano.ciudad = value.ciudad;
          ciudadano.telefono = value.telefono;
          ciudadano.direccion = value.direccion;
          ciudadano.email = value.email;
          ciudadano.cedula=value.cedula;
          this.ciudadano = ciudadano;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.ciudadanoService.getCiudadanoList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.cedula.localeCompare(value.cedula)==0){
          var key = document.id;
          this.ciudadanoService.updateCiudadano(key,this.ciudadano);
          Swal.fire('Se actualizo ciudadano con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }
}
