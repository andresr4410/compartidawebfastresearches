import { Component, OnInit } from '@angular/core';
import { RegionalService } from 'src/app/services/regional.service';
import { Regional } from 'src/app/models/regional';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-regional',
  templateUrl: './guardar-regional.component.html',
  styleUrls: ['./guardar-regional.component.css']
})
export class GuardarRegionalComponent implements OnInit {

  regional:Regional = new Regional();
  correcto = false;

  constructor(private regionalService:RegionalService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.regionalService.getregionalList().get().subscribe(response=>{
      this.regional.numeroRegional = response.docs.length+1;
      this.regionalService.createregional(this.regional);
      Swal.fire('Se creo la regional con exito !! ', "Todo correcto ", 'success');
      this.regional = new Regional();
    });
  }

  onSubmit(){
    this.save();
  }

  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

}
