import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { TipoUsuario } from 'src/app/models/tipoUsuario';
import { MatPaginator } from '@angular/material/paginator';
import { TipoUsuarioService } from 'src/app/services/tipo-usuario.service';
import { MatSort } from '@angular/material/sort';
import { MatSnackBar } from '@angular/material/snack-bar';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tipo-usuario-list',
  templateUrl: './tipo-usuario-list.component.html',
  styleUrls: ['./tipo-usuario-list.component.css']
})
export class TipoUsuarioListComponent implements OnInit {

  displayedColums:string[] = ['idTipoUsuario','Rol','opciones'];
  tipoUsuarios:any;
  public correcto:boolean=false;
  public nombre:string="";

  dataSource:MatTableDataSource<TipoUsuario>;
  @ViewChild(MatPaginator,{static:true}) paginator:MatPaginator;
  @ViewChild(MatSort,{static:true}) sort:MatSort;
  constructor(public tipoUsuarioService:TipoUsuarioService,
    public matSnackBar:MatSnackBar,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.inicializarLista();
  }

  inicializarLista(){
    this.tipoUsuarioService.getTipoUsuarioList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(tipoUsuarios=>{
      this.tipoUsuarios =tipoUsuarios;
      this.dataSource=new MatTableDataSource(this.tipoUsuarios);
      this.dataSource.paginator=this.paginator;
      this.dataSource.sort=this.sort;
    });
  }

  delete(idTipoUsuario:string,rol:string){
    this.tipoUsuarioService.getTipoUsuarioList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(idTipoUsuario == value.idTipoUsuario){
          var key = document.id;
          this.tipoUsuarioService.deleteTipoUsuario(key);
          Swal.fire('Se Elimino con exito !! ', "Todo correcto ", 'success');
          this.nombre =rol ;
        }
      });
    });
  }

  
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }


}
