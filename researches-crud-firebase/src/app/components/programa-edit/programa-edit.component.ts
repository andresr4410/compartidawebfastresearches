import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProgramaService } from 'src/app/services/programa.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-programa-edit',
  templateUrl: './programa-edit.component.html',
  styleUrls: ['./programa-edit.component.css']
})
export class ProgramaEditComponent implements OnInit {

  public programa:any;
  public numeroPrograma:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public programaService:ProgramaService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadPrograma();
  }

  loadPrograma(){
    let params=this.activatedRoute.params['_value'];
    this.numeroPrograma = params.programaNumero;
    var programa = {
      nombrePrograma:'',
      numeroPrograma:0,
      activo:true
    }
    programa.numeroPrograma = params.programaNumero;
    this.programaService.getProgramaList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroPrograma == value.numeroPrograma){
          programa.nombrePrograma = value.nombrePrograma;
          console.log(programa.nombrePrograma);
          this.programa = programa;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.programaService.getProgramaList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroPrograma == value.numeroPrograma){
          var key = document.id;
          this.programaService.updatePrograma(key,this.programa);
          Swal.fire('Se actualizo programa con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }


}
