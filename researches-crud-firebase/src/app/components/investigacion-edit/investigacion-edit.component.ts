import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InvestigacionService } from 'src/app/services/investigacion.service';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-investigacion-edit',
  templateUrl: './investigacion-edit.component.html',
  styleUrls: ['./investigacion-edit.component.css']
})
export class InvestigacionEditComponent implements OnInit {
  public investigacion:any;
  public numeroInvestigacion:string;
  public correcto:boolean = false;
  public incorrecto:boolean = false;
  constructor(public activatedRoute:ActivatedRoute,
    public investigacionService:InvestigacionService,
    public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    this.loadUsuario();
  }

  loadUsuario(){
    let params=this.activatedRoute.params['_value'];
    this.numeroInvestigacion = params.investigacionNumero;
    var investigacion = {
      nombreInvestigacion:'',
      numeroInvestigacion:0,
    }
    investigacion.numeroInvestigacion = params.investigacionNumero;
    console.log(params.investigacionNumero);
    this.investigacionService.getInvestigacionList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroInvestigacion == value.numeroInvestigacion){
          investigacion.nombreInvestigacion = value.nombreInvestigacion;
          console.log(investigacion.nombreInvestigacion);
          this.investigacion = investigacion;
        }
      });
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  actualizar(){
    this.investigacionService.getInvestigacionList().get().subscribe(response=>{
      response.docs.forEach(document=>{
        const value = document.data();
        if(this.numeroInvestigacion == value.numeroInvestigacion){
          var key = document.id;
          this.investigacionService.updateInvestigacion(key,this.investigacion);
          Swal.fire('Se actualizo investigacion con exito !! ', "Todo correcto ", 'success');
        }
      });
    });
  }

}
