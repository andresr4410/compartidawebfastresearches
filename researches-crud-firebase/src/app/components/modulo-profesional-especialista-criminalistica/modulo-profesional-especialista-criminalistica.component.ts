import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modulo-profesional-especialista-criminalistica',
  templateUrl: './modulo-profesional-especialista-criminalistica.component.html',
  styleUrls: ['./modulo-profesional-especialista-criminalistica.component.css']
})
export class ModuloProfesionalEspecialistaCriminalisticaComponent implements OnInit {
  sideBarOpen = true;
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  constructor(public authService:AuthService,
    public router:Router) { }


  ngOnInit(): void {
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }

  toggleSideBar(){
    this.sideBarOpen = !this.sideBarOpen;
  }

}
