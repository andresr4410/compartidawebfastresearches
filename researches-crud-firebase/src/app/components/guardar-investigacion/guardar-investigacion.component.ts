import { Component, OnInit } from '@angular/core';
import { Investigacion } from 'src/app/models/investigacion';
import { InvestigacionService } from 'src/app/services/investigacion.service';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-guardar-investigacion',
  templateUrl: './guardar-investigacion.component.html',
  styleUrls: ['./guardar-investigacion.component.css']
})
export class GuardarInvestigacionComponent implements OnInit {

  investigacion:Investigacion = new Investigacion();
  public correcto:boolean=false;
  public incorrecto:boolean = false;
  public tipoError:string = "";

  constructor(private investigacionService:InvestigacionService,
    public router:Router,
    public authService:AuthService) { }

  ngOnInit(): void {
  }

  save(){
    this.investigacionService.getInvestigacionList().get().subscribe(response=>{
      var contador = 0;
      var valor =1;
      while(contador<response.docs.length){
        response.forEach(document=>{
          if(valor ==document.data().numeroInvestigacion){
            valor+=1;
          }
        });
        contador+=1;
      }
      this.investigacion.numeroInvestigacion = valor;
      if(this.investigacion.numeroInvestigacion!=undefined && this.investigacion.nombreInvestigacion!=undefined){
        this.investigacionService.createInvestigacion(this.investigacion);
        this.investigacion = new Investigacion();
        Swal.fire('Se creo la investigacion con exito !! ', "Todo correcto ", 'success');
      }else{
        this.tipoError = "Error: Debe de digitar todos los campos";
        Swal.fire('No se creo la investigacion !! ',this.tipoError, 'error');
      }
    });
  }

  onSubmit(){
    this.save();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }


}
