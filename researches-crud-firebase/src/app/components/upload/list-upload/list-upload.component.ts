import { Component, OnInit} from '@angular/core';
import { FileServicesService } from 'src/app/services/file-services.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-upload',
  templateUrl: './list-upload.component.html',
  styleUrls: ['./list-upload.component.css']
})
export class ListUploadComponent implements OnInit {
 
  fileUploads: any[];
  constructor(private uploadService: FileServicesService) { }
 
  ngOnInit() {
    this.uploadService.getUploadList().snapshotChanges().pipe(
      map(changes=>
          changes.map(c=>
            ({key: c.payload.doc.id,  ...c.payload.doc.data()})
          )
        )
    ).subscribe(fileUploads=>{
      this.fileUploads = fileUploads;
    });
  }
}