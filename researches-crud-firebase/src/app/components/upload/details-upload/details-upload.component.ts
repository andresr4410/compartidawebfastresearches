import { Component, OnInit, Input } from '@angular/core';
import { FileUpload } from 'src/app/models/fileUpload';
import { FileServicesService } from 'src/app/services/file-services.service';
import { FileUpload2 } from 'src/app/models/fileUpload2';
import { Resultado } from 'src/app/models/resultado';

@Component({
  selector: 'app-details-upload',
  templateUrl: './details-upload.component.html',
  styleUrls: ['./details-upload.component.css']
})
export class DetailsUploadComponent implements OnInit {

  @Input() fileUpload: FileUpload;
  fileUpload2: FileUpload2;
  correcto:boolean =false;
 
  constructor(private uploadService: FileServicesService) { }
 
  ngOnInit() {
      var resultado = JSON.parse(localStorage.getItem(('usuario')));
      console.log(resultado.idTipoUsuario);
      if(resultado.idTipoUsuario == 2 ){
        this.correcto = true;
      }else{
        this.correcto = false;
      }
      this.fileUpload2 = new FileUpload2();
  }
 
  deleteFileUpload(fileUpload) {
    this.uploadService.getUploadList().get().subscribe(response=>{
      response.forEach(data=>{
        if(fileUpload.name.localeCompare(data.data().name)==0){
          this.fileUpload2.key = data.id;
          this.fileUpload2.name =fileUpload.name;
          this.fileUpload2.url = fileUpload.url;
          this.uploadService.deleteFileUpload(this.fileUpload2);
        }
      });
    });
    
  }
}
