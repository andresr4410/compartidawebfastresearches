import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-general-archivos',
  templateUrl: './general-archivos.component.html',
  styleUrls: ['./general-archivos.component.css']
})
export class GeneralArchivosComponent implements OnInit {
  sideBarOpen = false;
  correcto:boolean = false;
  @Output() toggleSideBarForMe: EventEmitter<any> = new EventEmitter();
  constructor(public authService:AuthService,
    public router:Router) { }

  ngOnInit(): void {
    var resultado = JSON.parse(localStorage.getItem(('usuario')));
      if(resultado.idTipoUsuario == 2 ){
        this.correcto = true;
      }else{
        this.correcto = false;
      }
  }


  logout() {
    this.authService.logout();
    this.router.navigate(['researchesLogin']);
    localStorage.clear();
  }
  regresar(){
    var usuario = {
      correo:'',
      idTipoUsuario:''
    }
    usuario =JSON.parse(localStorage.getItem('usuario'));
    if(usuario.idTipoUsuario.localeCompare("1")==0){
      this.router.navigate(['moduloAuxiliarAdministrativo']);
    }else if(usuario.idTipoUsuario.localeCompare("2")==0){
      this.router.navigate(['moduloIngeniero']);
    }else if(usuario.idTipoUsuario.localeCompare("3")==0){
      this.router.navigate(['moduloCoordinadorPag']);
    }else if(usuario.idTipoUsuario.localeCompare("4")==0){
      this.router.navigate(['moduloTecnicoCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("5")==0){
      this.router.navigate(['moduloProfesionalEspecializadoInvestigacion']);
    }else if(usuario.idTipoUsuario.localeCompare("6")==0){
      this.router.navigate(['moduloProfesionalEspecialistaCriminalistica']);
    }else if(usuario.idTipoUsuario.localeCompare("7")==0){
      this.router.navigate(['moduloDefensorPublico']);
    }
  }
  toggleSideBar(){
    this.sideBarOpen = !this.sideBarOpen;
  }
}
