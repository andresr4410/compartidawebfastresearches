import { Component, OnInit } from '@angular/core';
import { FileUpload } from 'src/app/models/fileUpload';
import { MisionFileServicesService } from 'src/app/services/mision-file-services.service';

@Component({
  selector: 'app-mision-form-upload',
  templateUrl: './mision-form-upload.component.html',
  styleUrls: ['./mision-form-upload.component.css']
})
export class MisionFormUploadComponent implements OnInit {
  selectedFilesMision: FileList;
  currentFileUploadMision: FileUpload;
  percentageMision: number;
  constructor(private uploadServiceMision: MisionFileServicesService) { }

  ngOnInit(): void {
  }

  selectFileMision(event) {
    this.selectedFilesMision = event.target.files;
  }
 

  uploadMision() {
    const file = this.selectedFilesMision.item(0);
    this.selectedFilesMision = undefined;
 
    this.currentFileUploadMision = new FileUpload(file);
    this.uploadServiceMision.pushFileToStorageMision(this.currentFileUploadMision).subscribe(
      percentage => {
        this.percentageMision = Math.round(percentage);
      },
      error => {
        
      }
    );
  }

}
