export class ItemInforme {
    key:string;
    idRegistro:string;
    idMision:string;
    description:string;
    fecha:string;
    evidencia:string;
    identificacionImagen:string;
    investigador:string;
    defensorPublico:string;
}
