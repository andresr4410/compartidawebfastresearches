export class DefensorPublico {
    key:string;
    nombre:string;
    apellido:string;
    cedula:number;
    arl:string;
    eps:string;
    contrato:number;
    direccion:string;
    email:string;
    pension:string;
    telefono:number;
}