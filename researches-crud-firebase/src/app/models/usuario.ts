export class Usuario {
    $key:string;
    idUsuario:number;
    idTipoUsuario:number;
    email:string;
    clave:string;
    activo=true;
    
    constructor(
        $key:string,
        idUsuario:number,
        idTipoUsuario:number,
        email:string,
        clave:string,
        activo=true
    ){}
}