export class PAG {
    key: string;
    cedula:number;
    nombre: string;
    apellido: string;
    direccion: string;
    email: string;
    telefono: number;
}
