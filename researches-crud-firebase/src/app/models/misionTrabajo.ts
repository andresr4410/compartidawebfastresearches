import { Estado } from './estado';

export class MisionTrabajo {
    key: string;
    radicadoInterno: string;
    misionTrabajo:string;
    programa:string;
    ciudadano:string;
    investigador:string;
    investigacion:string;
    SPOA:string;
    defensorPublico:string;
    pag:string;
    regional:string;
    fechaRecibido:Date;
    fechaAsignacion:Date;
    fechaDevolucion:Date;
    fechaDeInforme: Date;
    fechaVencimiento: Date;
    numeroInforme:number;
    estadoProceso:string;
    conductaPunible:string;
    resultado:string;
    observaciones:string;
    diasVencimiento:number;
    estado:string;
    estadoActivo:boolean;
}
