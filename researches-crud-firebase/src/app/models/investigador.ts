export class Investigador {
    key: string;
    nombre: string;
    apellido: string;
    cedula: number;
    cargo: string;
    direccion: string;
    email: string;
    telefono: number;
    profesion: string;
    observacion: string;
}
