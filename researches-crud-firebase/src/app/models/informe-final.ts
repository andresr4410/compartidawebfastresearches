export class InformeFinal {
    key:string;
    idMision:string;
    evidencia:string;
    identificacionImagen:string;
    defensorPublico:string;
    investigador:string;
    description:string;
    fecha:string;
}
