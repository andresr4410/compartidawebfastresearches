export class EstadoProceso {
    key: string;
    numeroProceso: number;
    nombreProceso: string;
}
