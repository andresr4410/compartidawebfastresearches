export class Ciudadano {
    key: string;
    nombre:string;
    apellido:string;
    cedula: number;
    edad: number;
    direccion: string;
    telefono: number;
    ciudad: string;
    email: string;
}
