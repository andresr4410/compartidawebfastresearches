import {NgModule, Component } from '@angular/core';
import {Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {FilemanagamentComponent } from './components/filemanagament/filemanagament.component';
import { GuardarUsuarioComponent } from './components/guardar-usuario/guardar-usuario.component';
import { UsuarioListComponent } from './components/usuario-list/usuario-list.component';
import { GuardarDefensorComponent } from './components/guardar-defensor/guardar-defensor.component';
import { GuardarProgramaComponent } from './components/guardar-programa/guardar-programa.component';
import { GuardarConductaPunibleComponent } from './components/guardar-conducta-punible/guardar-conducta-punible.component';
import { GuardarRegionalComponent } from './components/guardar-regional/guardar-regional.component';
import { GuardarEstadoProcesoComponent } from './components/guardar-estado-proceso/guardar-estado-proceso.component';
import { GuardarInvestigacionComponent } from './components/guardar-investigacion/guardar-investigacion.component';
import { GuardarResultadoComponent } from './components/guardar-resultado/guardar-resultado.component';
import { GuardarCiudadanoComponent } from './components/guardar-ciudadano/guardar-ciudadano.component';
import { GuardarInvestigadorComponent } from './components/guardar-investigador/guardar-investigador.component';
import { ModuloIngenieroComponent } from './components/modulo-ingeniero/modulo-ingeniero.component';
import { DefensorListComponent } from './components/defensor-list/defensor-list.component';
import { InvestigadorListComponent } from './components/investigador-list/investigador-list.component';
import { CiudadanoListComponent } from './components/ciudadano-list/ciudadano-list.component';
import { InvestigacionListComponent } from './components/investigacion-list/investigacion-list.component';
import { ConductaPunibleListComponent } from './components/conducta-punible-list/conducta-punible-list.component';
import { ProgramaListComponent } from './components/programa-list/programa-list.component';
import { EstadoProcesoListComponent } from './components/estado-proceso-list/estado-proceso-list.component';
import { ResultadoListComponent } from './components/resultado-list/resultado-list.component';
import { GuardarTipoUsuarioComponent } from './components/guardar-tipo-usuario/guardar-tipo-usuario.component';
import { TipoUsuarioListComponent } from './components/tipo-usuario-list/tipo-usuario-list.component';
import { PagListComponent } from './components/pag-list/pag-list.component';
import { GuardarPAGComponent } from './components/guardar-pag/guardar-pag.component';
import { ModuloAuxiliarAdministrativoComponent } from './components/modulo-auxiliar-administrativo/modulo-auxiliar-administrativo.component';
import { ModuloCoordinadorPagComponent } from './components/modulo-coordinador-pag/modulo-coordinador-pag.component';
import { ModuloDefensorPublicoComponent } from './components/modulo-defensor-publico/modulo-defensor-publico.component';
import { ModuloProfesionalEspecialistaCriminalisticaComponent } from './components/modulo-profesional-especialista-criminalistica/modulo-profesional-especialista-criminalistica.component';
import { ModuloProfesionalEspecializadoInvestigacionComponent } from './components/modulo-profesional-especializado-investigacion/modulo-profesional-especializado-investigacion.component';
import { ModuloTecnicoCriminalisticaComponent } from './components/modulo-tecnico-criminalistica/modulo-tecnico-criminalistica.component';
import { AuthenticationGuard } from './guards/authentication.guard';
import { UsuarioEditComponent } from './components/usuario-edit/usuario-edit.component';
import { TipoUsuarioEditComponent } from './components/tipo-usuario-edit/tipo-usuario-edit.component';
import { ConductaPunibleEditComponent } from './components/conducta-punible-edit/conducta-punible-edit.component';
import { ProgramaEditComponent } from './components/programa-edit/programa-edit.component';
import { InvestigacionEditComponent } from './components/investigacion-edit/investigacion-edit.component';
import { EstadoProcesoEditComponent } from './components/estado-proceso-edit/estado-proceso-edit.component';
import { ResultadoEditComponent } from './components/resultado-edit/resultado-edit.component';
import { DefensorPublicoEditComponent } from './components/defensor-publico-edit/defensor-publico-edit.component';
import { InvestigadorEditComponent } from './components/investigador-edit/investigador-edit.component';
import { CiudadanoEditComponent } from './components/ciudadano-edit/ciudadano-edit.component';
import { PagEditComponent } from './components/pag-edit/pag-edit.component';
import { MisionTrabajoComponent } from './components/mision-trabajo/mision-trabajo.component';
import { RegionalListComponent } from './components/regional-list/regional-list.component';
import { RegionalEditComponent } from './components/regional-edit/regional-edit.component';
import { MisionTrabajoListComponent } from './components/mision-trabajo-list/mision-trabajo-list.component';
import { MisionTrabajoEditComponent } from './components/mision-trabajo-edit/mision-trabajo-edit.component';
import { ConsultarMisionComponent } from './components/consultar-mision/consultar-mision.component';
import { MisionesCorreoComponent } from './components/misiones-correo/misiones-correo.component';
import { ListUploadComponent } from './components/upload/list-upload/list-upload.component';
import { FormUploadComponent } from './components/upload/form-upload/form-upload.component';
import { DetailsUploadComponent } from './components/upload/details-upload/details-upload.component';
import { GeneralArchivosComponent } from './components/upload/general-archivos/general-archivos.component';
import { ConsultarMisionDefensorComponent } from './components/consultar-mision-defensor/consultar-mision-defensor.component';
import { DoughnutChartComponent } from './components/doughnut-chart/doughnut-chart.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { BarraComponent } from './components/barra/barra.component';
import { FiltrarMisionesComponent } from './components/filtrar-misiones/filtrar-misiones.component';
import { InformeFinalComponent } from './components/informe-final/informe-final.component';
import { SeleccionarInformeFinalComponent } from './components/seleccionar-informe-final/seleccionar-informe-final.component';
import { ConsultarInformeFinalComponent } from './components/consultar-informe-final/consultar-informe-final.component';
import { RevisarEvidenciaInformeFinalComponent } from './components/revisar-evidencia-informe-final/revisar-evidencia-informe-final.component';


const routes: Routes = [
  {path:'',component:LoginComponent},
  {path:'researchesLogin',component:LoginComponent},
  {path:'saveFile',component:FilemanagamentComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarUsuario',component:GuardarUsuarioComponent,canActivate:[AuthenticationGuard]},
  {path:'listaUsuario',component:UsuarioListComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarPrograma',component:GuardarProgramaComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarDefensor',component:GuardarDefensorComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarConductaPunible',component:GuardarConductaPunibleComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarRegional',component:GuardarRegionalComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarEstadoProceso',component:GuardarEstadoProcesoComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarInvestigacion',component:GuardarInvestigacionComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarResultado',component:GuardarResultadoComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarCiudadano',component:GuardarCiudadanoComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarInvestigador',component:GuardarInvestigadorComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarTipoUsuario',component:GuardarTipoUsuarioComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarPAG',component:GuardarPAGComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloIngeniero',component:ModuloIngenieroComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloAuxiliarAdministrativo',component:ModuloAuxiliarAdministrativoComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloCoordinadorPag',component:ModuloCoordinadorPagComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloDefensorPublico',component:ModuloDefensorPublicoComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloProfesionalEspecialistaCriminalistica',component:ModuloProfesionalEspecialistaCriminalisticaComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloProfesionalEspecializadoInvestigacion',component:ModuloProfesionalEspecializadoInvestigacionComponent,canActivate:[AuthenticationGuard]},
  {path:'moduloTecnicoCriminalistica',component:ModuloTecnicoCriminalisticaComponent,canActivate:[AuthenticationGuard]},
  {path:'listaDefensor',component:DefensorListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaInvestigador',component:InvestigadorListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaCiudadano',component:CiudadanoListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaInvestigacion',component:InvestigacionListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaConducta',component:ConductaPunibleListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaPrograma',component:ProgramaListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaEstadoProceso',component:EstadoProcesoListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaResultado',component:ResultadoListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaTipoUsuario',component:TipoUsuarioListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaPag',component:PagListComponent,canActivate:[AuthenticationGuard]},
  {path:'listaRegional',component:RegionalListComponent,canActivate:[AuthenticationGuard]},
  {path:'usuarioEdit/:usuarioId',component:UsuarioEditComponent,canActivate:[AuthenticationGuard]},
  {path:'tipoUsuarioEdit/:tipoUsuarioId',component:TipoUsuarioEditComponent,canActivate:[AuthenticationGuard]},
  {path:'conductaPunibleEdit/:conductaPunibleNumero',component:ConductaPunibleEditComponent,canActivate:[AuthenticationGuard]},
  {path:'programaEdit/:programaNumero',component:ProgramaEditComponent,canActivate:[AuthenticationGuard]},
  {path:'investigacionEdit/:investigacionNumero',component:InvestigacionEditComponent,canActivate:[AuthenticationGuard]},
  {path:'estadoProcesoEdit/:estadoProcesoNumero',component:EstadoProcesoEditComponent,canActivate:[AuthenticationGuard]},
  {path:'resultadoEdit/:resultadoNumero',component:ResultadoEditComponent,canActivate:[AuthenticationGuard]},
  {path:'defensorPublicoEdit/:cedulaDefensor',component:DefensorPublicoEditComponent,canActivate:[AuthenticationGuard]},
  {path:'investigadorEdit/:cedulaInvestigador',component:InvestigadorEditComponent,canActivate:[AuthenticationGuard]},
  {path:'ciudadanoEdit/:cedulaCiudadano',component:CiudadanoEditComponent,canActivate:[AuthenticationGuard]},
  {path:'pagEdit/:cedulaPag',component:PagEditComponent,canActivate:[AuthenticationGuard]},
  {path:'misionTrabajoEdit/:numeroRadicado',component:MisionTrabajoEditComponent,canActivate:[AuthenticationGuard]},
  {path:'regionalEdit/:regionalNumero',component:RegionalEditComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarMisionTrabajo',component:MisionTrabajoComponent,canActivate:[AuthenticationGuard]},
  {path:'listaMisionTrabajo',component:MisionTrabajoListComponent,canActivate:[AuthenticationGuard]},
  {path:'consultarMision',component:ConsultarMisionComponent,canActivate:[AuthenticationGuard]},
  {path:'consultarMisionCorreo',component:MisionesCorreoComponent,canActivate:[AuthenticationGuard]},
  {path:'listaArchivos',component:ListUploadComponent,canActivate:[AuthenticationGuard]},
  {path:'guardarArchivo',component:FormUploadComponent,canActivate:[AuthenticationGuard]},
  {path:'detallesArchivo',component:DetailsUploadComponent,canActivate:[AuthenticationGuard]},
  {path:'generalArchivos',component:GeneralArchivosComponent,canActivate:[AuthenticationGuard]},
  {path:'consultarMisionDefensor',component:ConsultarMisionDefensorComponent,canActivate:[AuthenticationGuard]},
  {path:'informesEstadisticosDefensor',component:PieChartComponent,canActivate:[AuthenticationGuard]},
  {path:'informesEstadisticosIngeniero',component:BarraComponent,canActivate:[AuthenticationGuard]},
  {path:'filtrarMisiones',component:FiltrarMisionesComponent,canActivate:[AuthenticationGuard]},
  {path:'informeFinal/:idMision',component:InformeFinalComponent,canActivate:[AuthenticationGuard],runGuardsAndResolvers: 'always'},
  {path:'seleccionarInformeFinal',component:SeleccionarInformeFinalComponent,canActivate:[AuthenticationGuard]},
  {path:'consultarInformeFinal',component:ConsultarInformeFinalComponent,canActivate:[AuthenticationGuard]},
  {path:'revisarEvidenciaInformeFinal/:idMision',component:RevisarEvidenciaInformeFinalComponent,canActivate:[AuthenticationGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
