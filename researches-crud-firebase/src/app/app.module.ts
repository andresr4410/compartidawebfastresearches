import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
//firebase
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireStorage} from 'angularfire2/storage'
import {environment} from '../environments/environment';
import { AngularFireStorageModule } from '@angular/fire/storage';
import {AngularFirestore,FirestoreSettingsToken,AngularFirestoreModule} from '@angular/fire/firestore';

//GRAFICOS NG-SHARTS
import { ChartsModule } from 'ng2-charts';
//components 
import { FilemanagamentComponent } from './components/filemanagament/filemanagament.component';

//services 
import { AuthService } from '../app/services/auth.service';
import { LoginComponent } from './components/login/login.component';

//formulario
import { ReactiveFormsModule } from '@angular/forms';
import { GuardarUsuarioComponent } from './components/guardar-usuario/guardar-usuario.component';


//Angular material
import {FlexLayoutModule } from '@angular/flex-layout';
import {MatSliderModule } from '@angular/material/slider';
import {MatListModule} from '@angular/material/list';
import { MatDrawer,MatDrawerContent, MatSidenavModule, MatDrawerContainer} from '@angular/material/sidenav';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule } from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import { UsuarioListComponent } from './components/usuario-list/usuario-list.component';
import { GuardarDefensorComponent } from './components/guardar-defensor/guardar-defensor.component';
import { GuardarProgramaComponent } from './components/guardar-programa/guardar-programa.component';
import { GuardarConductaPunibleComponent } from './components/guardar-conducta-punible/guardar-conducta-punible.component';
import { GuardarEstadoProcesoComponent } from './components/guardar-estado-proceso/guardar-estado-proceso.component';
import { GuardarRegionalComponent } from './components/guardar-regional/guardar-regional.component';
import { GuardarInvestigacionComponent } from './components/guardar-investigacion/guardar-investigacion.component';
import { GuardarResultadoComponent } from './components/guardar-resultado/guardar-resultado.component';
import { GuardarCiudadanoComponent } from './components/guardar-ciudadano/guardar-ciudadano.component';
import { GuardarInvestigadorComponent } from './components/guardar-investigador/guardar-investigador.component';
import { ModuloIngenieroComponent } from './components/modulo-ingeniero/modulo-ingeniero.component';
import { DefensorListComponent } from './components/defensor-list/defensor-list.component';
import { InvestigadorListComponent } from './components/investigador-list/investigador-list.component';
import { CiudadanoListComponent } from './components/ciudadano-list/ciudadano-list.component';
import { InvestigacionListComponent } from './components/investigacion-list/investigacion-list.component';
import { ConductaPunibleListComponent } from './components/conducta-punible-list/conducta-punible-list.component';
import { ProgramaListComponent } from './components/programa-list/programa-list.component';
import { EstadoProcesoListComponent } from './components/estado-proceso-list/estado-proceso-list.component';
import { ResultadoListComponent } from './components/resultado-list/resultado-list.component';
import { GuardarTipoUsuarioComponent } from './components/guardar-tipo-usuario/guardar-tipo-usuario.component';
import { TipoUsuarioListComponent } from './components/tipo-usuario-list/tipo-usuario-list.component';
import { GuardarPAGComponent } from './components/guardar-pag/guardar-pag.component';
import { PagListComponent } from './components/pag-list/pag-list.component';
import { ModuloAuxiliarAdministrativoComponent } from './components/modulo-auxiliar-administrativo/modulo-auxiliar-administrativo.component';
import { ModuloProfesionalEspecializadoInvestigacionComponent } from './components/modulo-profesional-especializado-investigacion/modulo-profesional-especializado-investigacion.component';
import { ModuloDefensorPublicoComponent } from './components/modulo-defensor-publico/modulo-defensor-publico.component';
import { ModuloProfesionalEspecialistaCriminalisticaComponent } from './components/modulo-profesional-especialista-criminalistica/modulo-profesional-especialista-criminalistica.component';
import { ModuloCoordinadorPagComponent } from './components/modulo-coordinador-pag/modulo-coordinador-pag.component';
import { ModuloTecnicoCriminalisticaComponent } from './components/modulo-tecnico-criminalistica/modulo-tecnico-criminalistica.component';
import { UsuarioEditComponent } from './components/usuario-edit/usuario-edit.component';
import { TipoUsuarioEditComponent } from './components/tipo-usuario-edit/tipo-usuario-edit.component';
import { ConductaPunibleEditComponent } from './components/conducta-punible-edit/conducta-punible-edit.component';
import { ProgramaEditComponent } from './components/programa-edit/programa-edit.component';
import { InvestigacionEditComponent } from './components/investigacion-edit/investigacion-edit.component';
import { EstadoProcesoEditComponent } from './components/estado-proceso-edit/estado-proceso-edit.component';
import { ResultadoEditComponent } from './components/resultado-edit/resultado-edit.component';
import { DefensorPublicoEditComponent } from './components/defensor-publico-edit/defensor-publico-edit.component';
import { InvestigadorEditComponent } from './components/investigador-edit/investigador-edit.component';
import { CiudadanoEditComponent } from './components/ciudadano-edit/ciudadano-edit.component';
import { PagEditComponent } from './components/pag-edit/pag-edit.component';
import { MisionTrabajoComponent } from './components/mision-trabajo/mision-trabajo.component';
import { RegionalListComponent } from './components/regional-list/regional-list.component';
import { RegionalEditComponent } from './components/regional-edit/regional-edit.component';
import { MisionTrabajoListComponent } from './components/mision-trabajo-list/mision-trabajo-list.component';
import { MisionTrabajoEditComponent } from './components/mision-trabajo-edit/mision-trabajo-edit.component';
import { ConsultarMisionComponent } from './components/consultar-mision/consultar-mision.component';
import { MisionesCorreoComponent } from './components/misiones-correo/misiones-correo.component';
import { FormUploadComponent } from './components/upload/form-upload/form-upload.component';
import { DetailsUploadComponent } from './components/upload/details-upload/details-upload.component';
import { ListUploadComponent } from './components/upload/list-upload/list-upload.component';
import { GeneralArchivosComponent } from './components/upload/general-archivos/general-archivos.component';
import { MisionFormUploadComponent } from './components/upload/mision-form-upload/mision-form-upload.component';
import { ConsultarMisionDefensorComponent } from './components/consultar-mision-defensor/consultar-mision-defensor.component';
import { DoughnutChartComponent } from './components/doughnut-chart/doughnut-chart.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { BarraComponent } from './components/barra/barra.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FiltrarMisionesComponent } from './components/filtrar-misiones/filtrar-misiones.component';
import { InformeFinalComponent } from './components/informe-final/informe-final.component';
import { SeleccionarInformeFinalComponent } from './components/seleccionar-informe-final/seleccionar-informe-final.component';
import { ConsultarInformeFinalComponent } from './components/consultar-informe-final/consultar-informe-final.component';
import { RevisarEvidenciaInformeFinalComponent } from './components/revisar-evidencia-informe-final/revisar-evidencia-informe-final.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FilemanagamentComponent,
    GuardarUsuarioComponent,
    UsuarioListComponent,
    GuardarDefensorComponent,
    GuardarProgramaComponent,
    GuardarConductaPunibleComponent,
    GuardarEstadoProcesoComponent,
    GuardarRegionalComponent,
    GuardarInvestigacionComponent,
    GuardarResultadoComponent,
    GuardarCiudadanoComponent,
    GuardarInvestigadorComponent,
    ModuloIngenieroComponent,
    DefensorListComponent,
    InvestigadorListComponent,
    CiudadanoListComponent,
    InvestigacionListComponent,
    ConductaPunibleListComponent,
    ProgramaListComponent,
    EstadoProcesoListComponent,
    ResultadoListComponent,
    GuardarTipoUsuarioComponent,
    TipoUsuarioListComponent,
    GuardarPAGComponent,
    PagListComponent,
    ModuloAuxiliarAdministrativoComponent,
    ModuloProfesionalEspecializadoInvestigacionComponent,
    ModuloDefensorPublicoComponent,
    ModuloProfesionalEspecialistaCriminalisticaComponent,
    ModuloCoordinadorPagComponent,
    ModuloTecnicoCriminalisticaComponent,
    UsuarioEditComponent,
    TipoUsuarioEditComponent,
    ConductaPunibleEditComponent,
    ProgramaEditComponent,
    InvestigacionEditComponent,
    EstadoProcesoEditComponent,
    ResultadoEditComponent,
    DefensorPublicoEditComponent,
    InvestigadorEditComponent,
    CiudadanoEditComponent,
    PagEditComponent,
    MisionTrabajoComponent,
    RegionalListComponent,
    RegionalEditComponent,
    MisionTrabajoListComponent,
    MisionTrabajoEditComponent,
    ConsultarMisionComponent,
    MisionesCorreoComponent,
    FormUploadComponent,
    DetailsUploadComponent,
    ListUploadComponent,
    GeneralArchivosComponent,
    MisionFormUploadComponent,
    ConsultarMisionDefensorComponent,
    DoughnutChartComponent,
    PieChartComponent,
    BarraComponent,
    FiltrarMisionesComponent,
    InformeFinalComponent,
    SeleccionarInformeFinalComponent,
    ConsultarInformeFinalComponent,
    RevisarEvidenciaInformeFinalComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AppRoutingModule,
    FormsModule,
    AngularFireDatabaseModule,
    BrowserAnimationsModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    ReactiveFormsModule,
    AngularFirestoreModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule,
    MatMenuModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatSelectModule,
    MatCardModule,
    MatSliderModule,
    MatToolbarModule,
    MatAutocompleteModule,
    MatDividerModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatDividerModule,
    MatListModule,
    ChartsModule,
    NgbModule
  ],
  providers: [
    AuthService,
    AngularFirestore,
    [{provide:FirestoreSettingsToken,useValue:{}}]
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
